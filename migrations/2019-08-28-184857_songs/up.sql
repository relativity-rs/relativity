create table songs (
             id text primary key not null,
             location text not null unique,
             title text not null,
             album_id text not null,
             year integer not null,
             track text not null,
             genre text not null,
             bitrate integer not null,
             length integer not null
         )