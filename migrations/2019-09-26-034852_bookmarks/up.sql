create table bookmarks (
                    id integer primary key not null,
                    file_id integer not null,
                    user integer not null,
                    position int not null,
                    comment string not null
                 )