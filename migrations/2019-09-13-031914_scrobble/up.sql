create table scrobble (
                    id integer primary key not null,
                    last_played integer not null,
                    amount_played integer not null,
                    when_added integer not null,
                    rating int not null,
                    amount_star int not null
                 )