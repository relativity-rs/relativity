create table playlists (
             id text primary key not null,
             name text not null,
             songs text not null,
             date text not null,
             user_id text not null,
             comment text not null,
             public integer not null
         )