# Relativity

A rust reimplementation of subsonic - a web based media streamer.


## Installation

### Requirements

sqlite development libraries

### Installation


    git clone https://gitlab.com/relativity-rs/relativity
    cargo build --release

### Running

It's in Alpha mode, so the DB layout may change between upgrades, so you should delete your `state.db` file when upgrading.

    relativity  --db_file $STATE_DB_FILE --music_dir $HOME_MUSIC_DIR --password_file $PASSWORD_FILE --address $ADDRESS

where

* `$STATE_DB_FILE` - The file where relativity can hold its cache and program state(playlists and regular users), creating it if it doesn't exist. relativity must be have read/write access to the file.
* `$HOME_MUSIC_DIR` - The directory where the music files are located. relativity must have read/execute access to the directory, and to the files in that directory.
* `$PASSWORD_FILE` - A file which contains the admin usernames and passwords, user per line, where each entry is in the form of "USERNAME|PASSWORD". Unfortunately, due to protocol limitations, **the password must be written in clear text**. relativity needs read access to this file.
* `$ADDRESS` - The server's binding address:port (like `0.0.0.0:8080`).

## Current status

### General

However, there are several differences between relativity and subsonic:

1. As of now, it only works on mp3 and ogg audio (It don't support videos yet). The extension must be ".ogg" or ".mp3" (caps insensitive).
2. It ignores directory structure. The directory structure is automatically re-written to be "Artist/Album/Track" by id3 tag.
3. I don't have a last.fm account, so I don't expect to get that to work.
4. My music don't have album art, so I have no idea how to get that to work.
5. The web-based interface is planned, but for some time in the future.
6. I don't know of a good rust re-mixer, so it doesn't support that. It sends the audio files as-is, and doesn't send playlist times or song play times.
7. I store all the meta-data in memory. It works on my computer, but it should probably be loaded on-demand from the sqlite database.
8. I do a lot of String copying. I don't think it's that big of a deal, and should probably wait for point #7.
10. json or json-p is not yet supported.

I test it on Audinaut (0.3.2), and the basic functionality works.

#### System

| # | Feature    | XML     | JSON |
|---|------------|---------|------|
| 1 | Ping       | Yes     | Yes  |
| 2 | GetLicense | Yes     | Yes  |

#### Browsing

| #  | Feature           | Enabled      |
|----|-------------------|--------------|
| 1  | GetMusicFolders   | Yes          |
| 2  | GetIndexes        | Yes          |
| 3  | GetMusicDirectory | Yes          |
| 4  | GetGenres         | Yes          |
| 5  | GetArtists        | Yes          |
| 6  | GetArtist         | Yes          |
| 7  | GetAlbum          | Yes          |
| 8  | GetSong           | Yes          |
| 9  | GetVideos         | After MVP    |
| 10 | GetVideoInfo      | After MVP    |
| 11 | GetArtistInfo     | Not Planned  |
| 12 | GetArtistInfo     | Not Planned  |
| 13 | getAlbumInfo      | Not Planned  |
| 14 | getAlbumInfo2     | Not Planned  |
| 15 | getSimilarSongs   | Not Planned  |
| 16 | getSimilarSongs2  | Not Planned  |
| 17 | getTopSongs       | Not Planned  |

#### Album/song lists

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getAlbumList     | Yes          |
| 2 | getAlbumList2    | Yes          |
| 3 | getRandomSongs   | Yes          |
| 4 | getSongsByGenre  | Yes          |
| 5 | getNowPlaying    | After MVP    |
| 6 | getStarred       | After MVP    |
| 7 | getStarred2      | After MVP    |

#### Searching

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | search           | No           |
| 2 | search2          | Yes          |
| 3 | search3          | Yes          |

#### Playlists

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getPlaylists     | Yes          |
| 2 | getPlaylist      | Yes          |
| 3 | createPlaylist   | Yes          |
| 4 | updatePlaylist   | Yes          |
| 5 | deletePlaylist   | Yes          |

#### Media retrieval

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | stream           | Yes          |
| 2 | download         | Yes          |
| 3 | hls              | After MVP    |
| 4 | getCaptions      | After MVP    |
| 5 | getCoverArt      | After MVP    |
| 6 | getLyrics        | After MVP    |
| 7 | getAvatar        | After MVP    |

#### Media Annotation

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | star             | After MVP    |
| 2 | unstar           | After MVP    |
| 3 | setRating        | After MVP    |
| 4 | scrobble         | Yes          |

#### Sharing

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getShares        | After MVP    |
| 2 | createShare      | After MVP    |
| 3 | updateShare      | After MVP    |
| 4 | deleteShare      | After MVP    |

#### Podcast

| # | Feature                | Enabled      |
|---|------------------------|--------------|
| 1 | getPodcasts            | After MVP    |
| 2 | getNewestPodcasts      | After MVP    |
| 3 | refreshPodcasts        | After MVP    |
| 4 | createPodcastChannel   | After MVP    |
| 5 | deletePodcastChannel   | After MVP    |
| 6 | deletePodcastEpisode   | After MVP    |
| 7 | downloadPodcastEpisode | After MVP    |

#### Jukebox

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | jukeboxControl   | After MVP    |

#### Internet radio

| # | Feature                    | Enabled      |
|---|----------------------------|--------------|
| 1 | getInternetRadioStations   | After MVP    |
| 2 | createInternetRadioStation | After MVP    |
| 3 | updateInternetRadioStation | After MVP    |
| 4 | deleteInternetRadioStation | After MVP    |

#### Chat

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getChatMessages  | After MVP    |
| 2 | addChatMessage   | After MVP    |

#### User Management

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getUser          | After MVP    |
| 2 | getUsers         | After MVP    |
| 3 | createUser       | After MVP    |
| 4 | updateUser       | After MVP    |
| 5 | deleteUser       | After MVP    |
| 6 | changePassword   | After MVP    |

#### Bookmarks

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getBookmarks     | Yes          |
| 2 | createBookmark   | Yes          |
| 3 | deleteBookmark   | Yes          |
| 4 | getPlayQueue     | No           |
| 5 | savePlayQueue    | No           |

#### Scan

| # | Feature          | Enabled      |
|---|------------------|--------------|
| 1 | getScanStatus    | Yes          |
| 2 | startScan        | Yes          |

## License

MIT
