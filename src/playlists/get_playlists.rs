use crate::map;
use crate::models::User;
use crate::models::{Permission, Playlist};
use crate::prelude::*;
use std::convert::TryInto;

#[derive(Clone, Debug)]
pub struct Response {
    playlists: Vec<Playlist>,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, user: User) -> Result<Self, SubError> {
        let uid = get_hash(user.name.as_ref(), Type::User);
        let playlists = {
            let conn = crate::SQLITE_CONNECTION.lock().unwrap();

            let desired_name = req.iter().find_map(|x| {
                if x.0 == "username" {
                    Some(x.1.clone())
                } else {
                    None
                }
            });

            Playlist::get_all(&conn)?
                .into_iter()
                .filter(|x| {
                    x.user.id == uid
                        || (user.permissions == Permission::Admin
                            && desired_name.is_some()
                            && &x.user.name == desired_name.as_ref().unwrap())
                })
                .collect()
        };
        Ok(Self { playlists })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut playlists = XmlTag::new("playlists");

        for playlist in self.playlists {
            let song_count: i64 = playlist.songs.len().try_into().unwrap_or_default();
            let mut playlist_xml = XmlTag::new("playlist");
            playlist_xml.insert_attrs(map! {
            "id" => playlist.id,
            "comment" => playlist.comment,
            "name" => playlist.name,
            "owner" => playlist.user.name,
            "public" => playlist.public,
            "created" => playlist.date.to_string(),
            "songCount" => song_count
            });
            playlists.insert_child(playlist_xml);
        }

        return_val.insert_child(playlists);
        return_val.into_string()
    }
}
