use crate::diesel::Connection;
use crate::models::{Playlist, Song, User};
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
use chrono::prelude::*;

#[derive(Clone, Debug)]
pub struct Response {
    playlist: Playlist,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, user: User) -> Result<Self, SubError> {
        let conn = SQLITE_CONNECTION.lock().unwrap();
        (&conn)
            .transaction::<_, MyError, _>(|| {
                let users = User::get_all(&conn)?;
                let cur_user =
                    users
                        .into_iter()
                        .find(|u| u.name == user.name)
                        .ok_or_else(|| {
                            SubError::RequiredParameter(format!(
                                "{} {} user {} not found",
                                file!(),
                                line!(),
                                user.name
                            ))
                        })?;
                let playlist_id: Option<i64> = req
                    .iter()
                    .find(|x| x.0 == "playlistId")
                    .and_then(|x| (x.1.parse().ok()));
                let playlist_name = req
                    .iter()
                    .find(|x| x.0 == "name")
                    .and_then(|x| Some(x.1.clone()));

                let all_songs = Song::get_all(&conn)?;
                let songs: Vec<Song> = req
                    .into_iter()
                    .filter_map(|x| {
                        if x.0 == "songId" {
                            Some(x.1.parse::<i64>())
                        } else {
                            None
                        }
                    })
                    .collect::<Result<Vec<i64>, _>>()
                    .map_err(|e| {
                        SubError::GenericError(format!(
                            "{} {} songId {} not parseable",
                            file!(),
                            line!(),
                            e
                        ))
                    })?
                    .into_iter()
                    .map(|x| Some(all_songs.iter().find(|s| s.id == x)?.clone()))
                    .collect::<Option<Vec<Song>>>()
                    .ok_or_else(|| {
                        SubError::GenericError(format!("{} {} song not found", file!(), line!()))
                    })?;
                if let Some(playlist_name) = playlist_name {
                    let playlist_id = get_hash(playlist_name.as_ref(), Type::Playlist);
                    Playlist::delete_by_playlist_id(&conn, playlist_id)?;
                    let playlist = Playlist {
                        id: playlist_id,
                        name: playlist_name,
                        songs,
                        user: cur_user,
                        comment: "".to_string(),
                        date: Utc::now().naive_utc(),
                        public: false,
                    };
                    playlist.insert(&conn)?;
                    Ok(Self { playlist })
                } else if let Some(playlist_id) = playlist_id {
                    let playlist = Playlist::get_all(&conn)?
                        .into_iter()
                        .find(|x| x.id == playlist_id)
                        .ok_or_else(|| {
                            SubError::RequiredParameter(format!(
                                "{} {} Cant' find a playlist",
                                file!(),
                                line!()
                            ))
                        })?;
                    Playlist::delete_by_playlist_id(&conn, playlist_id)?;
                    let playlist = Playlist {
                        id: playlist_id,
                        name: playlist.name,
                        songs,
                        user: cur_user,
                        date: playlist.date,
                        comment: "".to_string(),
                        public: false,
                    };
                    playlist.insert(&conn)?;

                    Ok(Self { playlist })
                } else {
                    Err(MyError::SubError(SubError::GenericError(format!(
                        "{} {} must have either playlist id or name",
                        file!(),
                        line!(),
                    ))))
                }
            })
            .map_err(|e| e.into_sub_error(file!(), line!()))
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
