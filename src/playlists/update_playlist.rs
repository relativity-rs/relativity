use crate::diesel::Connection;
use crate::models::{Playlist, Song, User};
use crate::prelude::*;

#[derive(Clone, Debug)]
pub struct Response {}

impl New for Response {
    fn new(req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        let u_name = req
            .iter()
            .find(|x| x.0 == "u")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} u not found", file!(), line!()))
            })?
            .1
            .clone();
        let playlist_id: i64 = req
            .iter()
            .find(|x| x.0 == "playlistId")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!(
                    "{} {} playlist_id not found",
                    file!(),
                    line!()
                ))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        let playlist_name = req
            .iter()
            .find(|x| x.0 == "name")
            .and_then(|x| Some(x.1.clone()));

        let song_ids_to_add: Vec<i64> = req
            .iter()
            .filter_map(|x| {
                if x.0 == "songIdToAdd" {
                    Some(x.1.parse::<i64>())
                } else {
                    None
                }
            })
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let song_index_to_remove: Vec<usize> = req
            .iter()
            .filter_map(|x| {
                if x.0 == "songIndexToRemove" {
                    Some(x.1.parse().map_err(|e| {
                        SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                    }))
                } else {
                    None
                }
            })
            .collect::<Result<Vec<_>, SubError>>()?;
        let conn = &crate::SQLITE_CONNECTION.lock().unwrap();
        conn.transaction::<_, MyError, _>(|| {
            let mut playlist_to_modify = Playlist::get_all(conn)?
                .into_iter()
                .find(|p| p.id == playlist_id && p.has_permission_by_name(&u_name))
                .ok_or_else(|| SubError::UserNotAuthorized)?;
            if let Some(playlist_name) = playlist_name {
                Playlist::delete_by_playlist_id(conn, playlist_to_modify.id)?;
                playlist_to_modify.name = playlist_name;
                playlist_to_modify.insert(conn)?;

                Ok(Self {})
            } else if !song_ids_to_add.is_empty() {
                let songs = Song::get_all(&conn)?
                    .into_iter()
                    .filter(|x| song_ids_to_add.contains(&x.id));
                Playlist::delete_by_playlist_id(conn, playlist_to_modify.id)?;
                playlist_to_modify.songs.extend(songs);
                playlist_to_modify.insert(conn)?;

                Ok(Self {})
            } else if !song_index_to_remove.is_empty() {
                Playlist::delete_by_playlist_id(conn, playlist_to_modify.id)?;
                let songs: Vec<Song> = playlist_to_modify
                    .songs
                    .iter()
                    .enumerate()
                    .filter_map(|x| {
                        if song_index_to_remove.contains(&x.0) {
                            None
                        } else {
                            Some(x.1.clone())
                        }
                    })
                    .collect();
                playlist_to_modify.songs = songs;
                playlist_to_modify.insert(conn)?;
                Ok(Self {})
            } else {
                Err(MyError::from(SubError::GenericError(format!(
                    "{} {} No command sent",
                    file!(),
                    line!()
                ))))
            }
        })
        .map_err(|e| e.into_sub_error(file!(), line!()))
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
