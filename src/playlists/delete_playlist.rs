use crate::models::Permission;
use crate::models::Playlist;
use crate::models::User;
use crate::prelude::*;

#[derive(Clone, Debug)]
pub struct Response {}

impl New for Response {
    fn new(req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        let cur_user_name = req
            .iter()
            .find(|x| x.0 == "u")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} Userid not found", file!(), line!()))
            })?
            .1
            .as_ref();
        let playlist_id = req
            .iter()
            .find(|x| x.0 == "id")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} playlistid not found", file!(), line!()))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let conn = crate::SQLITE_CONNECTION.lock().unwrap();
        let cur_playlist = Playlist::get_all(&conn)?
            .into_iter()
            .find(|x| x.id == playlist_id)
            .ok_or_else(|| {
                SubError::RequestedDataNotFound(format!(
                    "{} {} Cant' find a playlist",
                    file!(),
                    line!()
                ))
            })?;

        if cur_playlist.user.name == cur_user_name
            || cur_playlist.user.permissions == Permission::Admin
        {
            Playlist::delete_by_playlist_id(&conn, playlist_id)?;
            Ok(Self {})
        } else {
            Err(SubError::UserNotAuthorized)
        }
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
