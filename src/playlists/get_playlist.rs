use crate::map;
use crate::models::Song;
use crate::models::User;
use crate::models::{Permission, Playlist};
use crate::prelude::*;
use std::convert::TryInto;

use chrono::NaiveDateTime;

#[derive(Clone, Debug)]
pub struct Response {
    id: i64,
    name: String,
    owner: String,
    created: NaiveDateTime,
    songs: Vec<Child>,
}

#[derive(Clone, Debug)]
struct Child {
    song: Song,
    suffix: String,
    content_type: &'static str,
    size: u64,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, user: User) -> Result<Self, SubError> {
        let playlist_id: i64 = req
            .iter()
            .find(|x| x.0 == "id")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!(
                    "{} {} playlist_id not found",
                    file!(),
                    line!()
                ))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        let uid = get_hash(user.name.as_ref(), Type::User);
        let playlist = {
            let conn = crate::SQLITE_CONNECTION.lock().unwrap();

            let desired_name = req.iter().find_map(|x| {
                if x.0 == "username" {
                    Some(x.1.clone())
                } else {
                    None
                }
            });

            Playlist::get_all(&conn)?
                .into_iter()
                .find(|x| {
                    x.id == playlist_id && x.user.id == uid
                        || (user.permissions == Permission::Admin
                            && desired_name.is_some()
                            && &x.user.name == desired_name.as_ref().unwrap())
                })
                .ok_or_else(|| {
                    SubError::RequestedDataNotFound(format!(
                        "{} {} Can't get playlist with id={}",
                        file!(),
                        line!(),
                        playlist_id
                    ))
                })?
        };
        Ok(Self {
            id: playlist_id,
            name: playlist.name,
            owner: playlist.user.name,
            created: playlist.date,
            songs: playlist
                .songs
                .into_iter()
                .map(|x| {
                    Ok(Child {
                        suffix: x.get_suffix(),
                        content_type: x.get_content_type()?,
                        size: x.get_file_size()?,
                        song: x.clone(),
                    })
                })
                .collect::<Result<Vec<_>, SubError>>()?,
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut playlist = XmlTag::new("playlist");
        let song_count: i64 = self.songs.len().try_into().unwrap_or_default();
        playlist.insert_attrs(map! {"id" => self.id,
        "name" => self.name,
        "owner" => self.owner,
        "songCount" => song_count,
        "created" => self.created});

        for child in self.songs {
            let mut song = XmlTag::new("entry");
            song.insert_attrs(child.song.into_xml_map());
            playlist.insert_child(song);
        }

        return_val.insert_child(playlist);
        return_val.into_string()
    }
}
