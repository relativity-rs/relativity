use crate::models::user::User;
use crate::prelude::*;

use std::sync::atomic::Ordering;

pub struct Response {
    cur_count: i64,
    is_scanning: bool,
}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        let cur_count = crate::CUR_COUNT.load(Ordering::Relaxed);
        let is_scanning = crate::IS_SCANNING.load(Ordering::Relaxed);
        Ok(Self {
            cur_count,
            is_scanning,
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut status_scanning = XmlTag::new("statusScanning");
        status_scanning.insert_attr("scanning", self.is_scanning);
        status_scanning.insert_attr("count", self.cur_count);
        return_val.insert_child(status_scanning);
        return_val.into_string()
    }
}
