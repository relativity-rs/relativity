use crate::models::user::User;
use crate::prelude::*;

use std::sync::atomic::Ordering;
use std::sync::mpsc::Sender;

pub struct Response {
    cur_count: i64,
    is_scanning: bool,
}

impl Response {
    pub fn new(
        _req: &[(String, String)],
        _users: &User,
        send_update_cache: &Sender<bool>,
    ) -> Result<Self, SubError> {
        send_update_cache
            .send(true)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let cur_count = crate::CUR_COUNT.load(Ordering::Relaxed);
        let is_scanning = crate::IS_SCANNING.load(Ordering::Relaxed);
        Ok(Self {
            cur_count,
            is_scanning,
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut status_scanning = XmlTag::new("statusScanning");
        status_scanning.insert_attr("scanning", self.is_scanning);
        status_scanning.insert_attr("count", self.cur_count);
        return_val.insert_child(status_scanning);
        return_val.into_string()
    }
}
