use crate::map;
use crate::models::Song;
use crate::prelude::*;
use std::collections::HashMap;

use std::fs;
use std::path::Path;

#[derive(Debug, PartialEq, Eq)]
pub struct Artist {
    pub id: i64,
    pub name: String,
    pub starred: Option<chrono::NaiveDateTime>,
}

pub fn get_artists(songs: Vec<Song>) -> HashMap<char, Vec<Artist>> {
    let mut indexes: HashMap<char, Vec<Artist>> = HashMap::new();
    songs.into_iter().for_each(|x| {
        let ch = x.album.artist.name.chars().next().unwrap_or(' ');
        let v = indexes.get_mut(&ch);
        if let Some(v) = v {
            let to_insert = Artist {
                id: x.album.artist.id,
                name: x.album.artist.name,
                starred: None,
            };
            if !v.contains(&to_insert) {
                v.push(to_insert);
            }
        } else {
            indexes.insert(
                ch,
                vec![Artist {
                    id: x.album.artist.id,
                    name: x.album.artist.name,
                    starred: None,
                }],
            );
        }
    });
    indexes
}

#[derive(Debug)]
pub struct SongList {
    pub id: i64,
    pub parent: i64,
    pub title: String,
    pub album: String,
    pub artist: String,
    pub track: String,
    pub year: String,
    pub genre: String,
    pub size: u64,
    pub content_type: &'static str,
    pub suffix: String,
    pub path: String,
}

impl SongList {
    pub fn from(song: Song) -> Result<Self, SubError> {
        let p = Path::new(&song.location);
        let metadata =
            fs::metadata(p).map_err(|e| SubError::GenericError(format!("{}: {:?}", line!(), e)))?;
        let size = metadata.len();
        let suffix = p
            .extension()
            .ok_or_else(|| {
                SubError::GenericError(format!(
                    "{}: File {} has no extension",
                    line!(),
                    song.location
                ))
            })?
            .to_str()
            .ok_or_else(|| {
                SubError::GenericError(format!(
                    "{}: File {} is not UTF-8 compliant",
                    line!(),
                    song.location
                ))
            })?
            .to_owned();
        let content_type = extension_to_content_type(suffix.as_ref()).ok_or_else(|| {
            SubError::GenericError(format!(
                "{} {}'s content_type ({:?}) not found",
                line!(),
                song.id,
                p.extension()
            ))
        })?;

        Ok(Self {
            id: song.id,
            parent: song.album.id,
            title: song.title,
            album: song.album.title,
            artist: song.album.artist.name,
            track: song.track,
            year: song.year.to_string(),
            genre: song.genre,
            size,
            content_type,
            suffix,
            path: song.location.clone(),
        })
    }
    pub fn into_xml_map(self) -> HashMap<&'static str, SafeString> {
        map! {
                    "id" => self.id,
                    "parent" => self.parent,
                    "title" => self.title,
                    "album" => self.album,
                    "artist"=> self.artist,
                    "track" => if self.track != "" { self.track} else {String::from("0")},
                    "year" => self.year.to_string(),
                    "genre" => self.genre,
                    "path" => self.path,
                    "size" => self.size,
                    "contentType" =>self.content_type,
                    "suffix" => self.suffix
        }
    }
}
