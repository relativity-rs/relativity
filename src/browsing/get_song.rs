use super::prelude::SongList;
use crate::models::*;
use crate::prelude::*;

use crate::SQLITE_CONNECTION;

pub struct Response {
    song: SongList,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let get_song_id: i64 = req
            .iter()
            .find(|(id, _)| id == "id")
            .ok_or_else(|| SubError::RequiredParameter(format!("{}: id not found", line!())))?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let song = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            SongList::from(
                Song::get_all(&conn)?
                    .into_iter()
                    .find(|x| x.id == get_song_id)
                    .ok_or_else(|| {
                        SubError::RequestedDataNotFound(format!(
                            "{}: id {} not found",
                            line!(),
                            get_song_id
                        ))
                    })?,
            )?
        };

        Ok(Self { song })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut song = XmlTag::new("song");
        song.insert_attrs(self.song.into_xml_map());
        return_val.insert_child(song);

        return_val.into_string()
    }
}
