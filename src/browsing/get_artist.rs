use crate::map;
use crate::models::{Album, User};
use crate::prelude::New;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;

pub struct Response {
    albums: Vec<AlbumRes>,
    artist_id: i64,
    albums_len: u64,
    artist: String,
}

pub struct AlbumRes {
    id: i64,
    name: String,
    artist: String,
    artist_id: i64,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let get_artist_id: i64 = req
            .iter()
            .find(|(id, _)| id == "id")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {}: id not found", file!(), line!()))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {} ", file!(), line!(), e)))?;
        let albums: Vec<AlbumRes> = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Album::get_all(&conn)?
                .into_iter()
                .filter_map(|x| {
                    if x.artist.id == get_artist_id {
                        Some(AlbumRes {
                            id: x.id,
                            name: x.title,
                            artist: x.artist.name,
                            artist_id: x.artist.id,
                        })
                    } else {
                        None
                    }
                })
                .collect()
        };

        if albums.is_empty() {
            Err(SubError::RequestedDataNotFound(format!(
                "{} {}: artist id {} has 0 albums",
                file!(),
                line!(),
                get_artist_id
            )))
        } else {
            let artist_id = albums[0].artist_id;
            let artist = albums[0].artist.clone();
            let albums_len = albums.len() as u64;
            Ok(Self {
                artist,
                albums,
                artist_id,
                albums_len,
            })
        }
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut artist = XmlTag::new("artist");
        artist.insert_attrs(
            map! {"id" => self.artist_id, "name" => self.artist, "albumCount" => self.albums_len },
        );

        for album in self.albums {
            let mut albums = XmlTag::new("album");
            albums.insert_attrs(map! {
            "id" => album.id,
            "name" => album.name,
            "artistId" => album.artist_id,
            "artist" => album.artist
            });

            artist.insert_child(albums);
        }
        return_val.insert_child(artist);
        return_val.into_string()
    }
}
