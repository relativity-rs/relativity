use crate::browsing::prelude::SongList;
use crate::map;
use crate::models::*;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;

use std::convert::TryInto;

pub struct Response {
    album_id: i64,
    album_name: String,
    song_count: u64,
    artist: String,
    artist_id: i64,
    songs: Vec<SongList>,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let get_album_id: i64 = req
            .iter()
            .find(|(id, _)| id == "id")
            .ok_or_else(|| SubError::RequiredParameter(format!("{}: id not found", line!())))?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {} ", file!(), line!(), e)))?;
        let songs: Vec<Song> = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Song::get_all(&conn)?
                .into_iter()
                .filter(|x| x.album.id == get_album_id)
                .collect()
        };
        Ok(Self {
            album_id: songs
                .get(0)
                .and_then(|x| Some(x.album.id))
                .unwrap_or_default(),
            album_name: songs
                .get(0)
                .and_then(|x| Some(x.album.title.clone()))
                .unwrap_or_default()
                .clone(),
            song_count: songs
                .len()
                .try_into()
                .map_err(|e| SubError::GenericError(format!("{}: {:?}", line!(), e)))?,
            artist: songs
                .get(0)
                .and_then(|x| Some(x.album.artist.name.clone()))
                .unwrap_or_default()
                .clone(),
            artist_id: songs
                .get(0)
                .and_then(|x| Some(x.album.artist.id))
                .unwrap_or_default(),
            songs: songs
                .into_iter()
                .map(SongList::from)
                .collect::<Result<Vec<SongList>, SubError>>()?,
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut album = XmlTag::new("album");
        album.insert_attrs(map! {
        "id" => self.album_id,
        "name" => self.album_name,
        "songCount" => self.song_count,
        "artist" => self.artist,
        "artistId" => self.artist_id
        });

        for song in self.songs {
            let mut child = XmlTag::new("song");
            child.insert_attrs(song.into_xml_map());
            child.insert_attr("albumId", self.album_id);
            child.insert_attr("artistId", self.artist_id);
            album.insert_child(child);
        }

        return_val.insert_child(album);

        return_val.into_string()
    }
}
