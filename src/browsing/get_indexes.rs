use std::collections::HashMap;

use super::prelude::*;
use crate::map;
use crate::models::*;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;

use std::fs;
use std::path::Path;

#[derive(Debug)]
pub struct Response {
    last_modified: Option<i64>,
    indexes: HashMap<char, Vec<super::prelude::Artist>>,
    children: Vec<SongList>,
}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let songs = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Song::get_all(&conn)?
        };
        let indexes = get_artists(songs.clone());

        let children = songs
            .into_iter()
            .map(|x| {
                let loc = Path::new(&x.location);
                let md = fs::metadata(loc).map_err(|e| {
                    SubError::GenericError(format!(
                        "{} {}: {} : {}",
                        file!(),
                        line!(),
                        e,
                        loc.display()
                    ))
                })?;
                let size = md.len();
                let suffix = loc
                    .extension()
                    .unwrap_or_default()
                    .to_str()
                    .unwrap_or_default()
                    .to_string();
                let content_type = extension_to_content_type(
                    loc.extension()
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {}: File {} has no extension",
                                file!(),
                                line!(),
                                loc.display()
                            ))
                        })?
                        .to_str()
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {}: File {} is not UTF-8 compliant",
                                file!(),
                                line!(),
                                loc.display()
                            ))
                        })?,
                )
                .ok_or_else(|| {
                    SubError::GenericError(format!("{} {}: {}", file!(), line!(), loc.display()))
                })?;

                Ok(SongList {
                    id: x.id,
                    parent: x.album.id,
                    title: x.title,
                    album: x.album.title,
                    artist: x.album.artist.name,
                    track: x.track,
                    year: x.year.to_string(),
                    genre: x.genre,
                    size,
                    content_type,
                    suffix,
                    path: x.location,
                })
            })
            .collect::<Result<Vec<SongList>, SubError>>()?;

        Ok(Self {
            last_modified: None,
            indexes,
            children,
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut indexes = XmlTag::new("indexes");
        indexes.insert_attrs(map! {"lastModified" => self.last_modified.unwrap_or(237_462_836_472_342).to_string(), "ignoredArticles" => ""});

        for i in &self.indexes {
            let mut index = XmlTag::new("index");
            index.insert_attr("name", i.0.to_string());

            for j in i.1 {
                let mut artist_elem = XmlTag::new("artist");
                artist_elem.insert_attrs(map! {"id" => j.id, "name" => j.name.clone()});
                index.insert_child(artist_elem);
            }
            indexes.insert_child(index);
        }
        return_val.insert_child(indexes);

        for child in self.children {
            let mut child_tag = XmlTag::new("child");
            child_tag.insert_attrs(child.into_xml_map());
            return_val.insert_child(child_tag);
        }

        return_val.into_string()
    }
}
