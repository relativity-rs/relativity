use crate::browsing::prelude::SongList;
use crate::map;
use crate::models::*;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
use std::convert::TryInto;

#[derive(Debug)]
pub struct Response {
    id: i64,
    parent: Option<i64>,
    name: String,
    children: Vec<Child>,
}

#[derive(Debug)]
enum Child {
    DirList(DirList),
    SongList(Box<SongList>),
}

#[derive(Debug)]
struct DirList {
    id: i64,
    name: String,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let id: i64 = req
            .iter()
            .find(|(id, _)| id == "id")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} id not passed", file!(), line!()))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let t = id.try_into()?;

        match t {
            Type::Playlist | Type::User | Type::Queue => Err(SubError::GenericError(format!(
                "Didn't expect {:?} {}",
                t, id
            ))),
            Type::TopLevel => {
                let artists = {
                    let conn = SQLITE_CONNECTION.lock().unwrap();
                    Artist::get_all(&conn)?
                        .into_iter()
                        .map(|x| {
                            Child::DirList(DirList {
                                id: x.id,
                                name: x.name,
                            })
                        })
                        .collect()
                };

                Ok(Self {
                    id,
                    parent: None,
                    name: "Music".to_string(),
                    children: artists,
                })
            }
            Type::Artist => {
                let (artist_name, albums) = {
                    let conn = SQLITE_CONNECTION.lock().unwrap();
                    let artist_name = Artist::get_all(&conn)?
                        .into_iter()
                        .find(|x| x.id == id)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} Cannot find id {}",
                                file!(),
                                line!(),
                                id
                            ))
                        })?
                        .name;

                    let albums = Album::get_all(&conn)?
                        .into_iter()
                        .filter_map(|x| {
                            if x.artist.id == id {
                                Some(Child::DirList(DirList {
                                    id: x.id,
                                    name: x.title,
                                }))
                            } else {
                                None
                            }
                        })
                        .collect();
                    (artist_name, albums)
                };
                Ok(Self {
                    id,
                    parent: Some(get_hash("", Type::TopLevel)),
                    name: artist_name,
                    children: albums,
                })
            }
            Type::Album => {
                let songs_arr = {
                    let conn = SQLITE_CONNECTION.lock().unwrap();
                    Song::get_all(&conn)?
                };
                let artist_id = songs_arr
                    .iter()
                    .find(|x| x.album.id == id)
                    .ok_or_else(|| {
                        SubError::RequestedDataNotFound(format!(
                            "{} {}: No songs which match id {}",
                            file!(),
                            line!(),
                            id
                        ))
                    })?
                    .album
                    .artist
                    .id;

                let album_name = songs_arr
                    .iter()
                    .find(|x| x.album.id == id)
                    .ok_or_else(|| {
                        SubError::RequestedDataNotFound(format!(
                            "{} {}: No songs which match id {}",
                            file!(),
                            line!(),
                            id
                        ))
                    })?
                    .album
                    .title
                    .clone();

                let songs = songs_arr
                    .into_iter()
                    .filter(|x| x.album.id == id)
                    .map(|x| Ok(Child::SongList(Box::new(SongList::from(x)?))))
                    .collect::<Result<Vec<Child>, SubError>>()?;
                Ok(Self {
                    id,
                    parent: Some(artist_id),
                    name: album_name,
                    children: songs,
                })
            }
            Type::Song | Type::Bookmark => panic!("Shouldn't get Song"),
        }
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();

        let mut directory = XmlTag::new("directory");
        directory.insert_attr("id", self.id);

        if let Some(parent) = self.parent {
            directory.insert_attr("parent", parent);
        }
        directory.insert_attr("name", self.name);

        let current_id = self.id;
        self.children.into_iter().for_each(|child| match child {
            Child::SongList(child) => {
                let mut child_tag = XmlTag::new("child");
                child_tag.insert_attrs(child.into_xml_map());
                directory.insert_child(child_tag);
            }
            Child::DirList(child) => {
                let mut child_tag = XmlTag::new("child");
                child_tag.insert_attrs(map! {
                  "id" => child.id,
                  "parent" => current_id,
                  "title" => child.name,
                  "isDir" => "true"
                });
                directory.insert_child(child_tag);
            }
        });
        return_val.insert_child(directory);

        return_val.into_string()
    }
}
