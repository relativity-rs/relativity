use crate::map;
use crate::models::User;
use crate::prelude::ContentType;
use crate::prelude::Printable;
use crate::prelude::Type;
use crate::prelude::*;

#[derive(Debug, Default)]
pub struct Response {}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        Ok(Self {})
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut music_folders = XmlTag::new("musicFolders");
        music_folders.insert_attrs(map! {"id" => get_hash("",Type::TopLevel), "name"=> "Music"});
        return_val.insert_child(music_folders);
        return_val.into_string()
    }
}
