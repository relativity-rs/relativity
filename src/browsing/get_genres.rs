use crate::models::*;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;

use std::collections::HashMap;

#[derive(Debug)]
pub struct Response {
    genres: Vec<Genre>,
}

#[derive(Debug)]
struct Genre {
    song_count: u64,
    name: String,
}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let songs: Vec<Song> = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Song::get_all(&conn)?
        };
        let mut hm = HashMap::new();
        songs.iter().for_each(|e| {
            let g = &e.genre;
            let song_count = *hm.get(g).unwrap_or(&0);
            hm.insert(g.to_string(), song_count + 1);
        });
        let genres = hm
            .into_iter()
            .map(|(name, song_count)| Genre { name, song_count })
            .collect();
        Ok(Self { genres })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut genres = XmlTag::new("genres");
        for genre in self.genres {
            let mut g = XmlTag::new("genre");
            g.insert_attr("songCount", genre.song_count);
            g.insert_val(genre.name);
            genres.insert_child(g)
        }
        return_val.insert_child(genres);
        return_val.into_string()
    }
}
