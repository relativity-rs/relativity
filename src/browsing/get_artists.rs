use super::prelude::*;
use crate::map;
use crate::models::{Song, User};
use crate::prelude::*;
use crate::prelude::{ContentType, Printable, XmlTag};
use crate::SQLITE_CONNECTION;

use std::collections::HashMap;

pub struct Response {
    indexes: HashMap<char, Vec<Artist>>,
}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let songs = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Song::get_all(&conn)?
        };
        Ok(Self {
            indexes: get_artists(songs),
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut artists_tag = XmlTag::new("artists");
        artists_tag.insert_attr("ignoredArticles", "");
        for i in self.indexes {
            let mut index = XmlTag::new("index");
            index.insert_attr("name", i.0.to_string());
            for j in i.1 {
                let mut artists = XmlTag::new("artist");
                artists.insert_attrs(map! {"id" => j.id, "name" => j.name});
                index.insert_child(artists);
            }
            artists_tag.insert_child(index)
        }
        return_val.insert_child(artists_tag);

        return_val.into_string()
    }
}
