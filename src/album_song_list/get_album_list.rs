use crate::map;
use crate::models::user::User;
use crate::models::Album;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
use std::cmp::Ordering;

pub struct Response {
    albums: Vec<AlbumList>,
}

#[derive(Debug, PartialEq, Eq, Ord)]
struct AlbumList {
    id: i64,
    parent: i64,
    title: String,
    artist: String,
}

impl PartialOrd for AlbumList {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let t = match req
            .iter()
            .find(|x| x.0 == "type")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} type not found", file!(), line!()))
            })?
            .1
            .as_ref()
        {
            "random" => Ok(ListType::Random),
            "newest" => Ok(ListType::Newest),
            "highest" => Ok(ListType::Highest),
            "frequent" => Ok(ListType::Frequent),
            "recent" => Ok(ListType::Recent),
            "alphabeticalByName" => Ok(ListType::AlphabeticalByName),
            "alphabeticalByArtist" => Ok(ListType::AlphabeticalByArtist),
            "starred" => Ok(ListType::Starred),
            "byYear" => Ok(ListType::ByYear),
            "byGenre" => Ok(ListType::ByGenre),

            p => Err(SubError::RequiredParameter(format!(
                "{} {} {} is not a known type",
                file!(),
                line!(),
                p
            ))),
        }?;
        let size: u16 = if let Some(size) = req.iter().find(|x| x.0 == "size") {
            size.1
                .parse()
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
        } else {
            10
        };

        let offset: u16 = if let Some(offset) = req.iter().find(|x| x.0 == "offset") {
            offset
                .1
                .parse()
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
        } else {
            0
        };

        let _from_year: Option<u16> =
            if let Some(from_year) = req.iter().find(|x| x.0 == "from_year") {
                Some(from_year.1.parse().map_err(|e| {
                    SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                })?)
            } else {
                None
            };

        let _to_year: Option<u16> =
            if let Some(to_year) = req.iter().find(|x| x.0 == "to_year") {
                Some(to_year.1.parse().map_err(|e| {
                    SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                })?)
            } else {
                None
            };

        let _genre = req
            .iter()
            .find(|x| x.0 == "genre")
            .and_then(|x| Some(x.1.clone()));
        let _music_folder_id = req
            .iter()
            .find(|x| x.0 == "musicFolderId")
            .and_then(|x| Some(x.1.clone()));

        let mut als: Vec<AlbumList> = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Album::get_iter(&conn)?
                .map(|x| {
                    let x = x?;
                    Ok(AlbumList {
                        id: x.id,
                        parent: x.artist.id,
                        title: x.title,
                        artist: x.artist.name,
                    })
                })
                .collect::<Result<Vec<_>, SubError>>()?
        };
        als.sort();
        als.dedup();

        match t {
            ListType::Recent
            | ListType::Starred
            | ListType::ByYear
            | ListType::ByGenre
            | ListType::Frequent
            | ListType::Highest
            | ListType::Newest
            | ListType::Random => als.sort_by(|x, y| x.id.partial_cmp(&y.id).unwrap()),

            ListType::AlphabeticalByName => {
                als.sort_by(|x, y| x.title.partial_cmp(&y.title).unwrap())
            }
            ListType::AlphabeticalByArtist => {
                als.sort_by(|x, y| x.artist.partial_cmp(&y.artist).unwrap())
            }
        };

        Ok(Self {
            albums: als
                .into_iter()
                .skip(offset as usize)
                .take(size as usize)
                .collect(),
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut album_tag = XmlTag::new("albumList");
        for al in self.albums {
            let mut al_tag = XmlTag::new("album");
            al_tag.insert_attrs(map! {
            "id" => al.id,
            "parent" => al.parent,
            "title" => al.title,
            "artist" => al.artist,
            "isDir" => "true"
            });
            album_tag.insert_child(al_tag);
        }
        return_val.insert_child(album_tag);
        return_val.into_string()
    }
}

#[derive(Debug, Copy, Clone)]
enum ListType {
    Random,
    Newest,
    Highest,
    Frequent,
    Recent,
    AlphabeticalByName,
    AlphabeticalByArtist,
    Starred,
    ByYear,
    ByGenre,
}
