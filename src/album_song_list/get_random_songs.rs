use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
use rand::seq::SliceRandom;
use rand::thread_rng;
pub struct Response {
    songs: Vec<Song>,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let size: u16 = if let Some(size) = req.iter().find(|x| x.0 == "size") {
            size.1
                .parse()
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
        } else {
            10
        };

        let _from_year: Option<u16> =
            if let Some(from_year) = req.iter().find(|x| x.0 == "from_year") {
                Some(from_year.1.parse().map_err(|e| {
                    SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                })?)
            } else {
                None
            };

        let _to_year: Option<u16> =
            if let Some(to_year) = req.iter().find(|x| x.0 == "to_year") {
                Some(to_year.1.parse().map_err(|e| {
                    SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                })?)
            } else {
                None
            };

        let _genre = req
            .iter()
            .find(|x| x.0 == "genre")
            .and_then(|x| Some(x.1.clone()));
        let _music_folder_id = req
            .iter()
            .find(|x| x.0 == "musicFolderId")
            .and_then(|x| Some(x.1.clone()));

        let mut songs: Vec<Song> = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            Song::get_all(&conn)?
        };
        songs.shuffle(&mut thread_rng());
        let songs = songs.into_iter().take(size as usize).collect();

        Ok(Self { songs })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut random_songs = XmlTag::new("randomSongs");
        for s in self.songs {
            let mut song_tag = XmlTag::new("song");
            song_tag.insert_attrs(s.into_xml_map());
            random_songs.insert_child(song_tag);
        }
        return_val.insert_child(random_songs);
        return_val.into_string()
    }
}
