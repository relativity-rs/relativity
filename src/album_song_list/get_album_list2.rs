use crate::map;
use crate::models::album::Album;
use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
use chrono::prelude::*;
use std::cmp::Ordering;

pub struct Response {
    albums: Vec<AlbumList>,
}

#[derive(Debug, PartialEq, Eq, Ord)]
struct AlbumList {
    id: i64,
    name: String,
    song_count: u64,
    created: String,
    artist: String,
    artist_id: i64,
}

impl PartialOrd for AlbumList {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let t = match req
            .iter()
            .find(|x| x.0 == "type")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} type not found", file!(), line!()))
            })?
            .1
            .as_ref()
        {
            "random" => Ok(ListType::Random),
            "newest" => Ok(ListType::Newest),
            "highest" => Ok(ListType::Highest),
            "frequent" => Ok(ListType::Frequent),
            "recent" => Ok(ListType::Recent),
            "alphabeticalByName" => Ok(ListType::AlphabeticalByName),
            "alphabeticalByArtist" => Ok(ListType::AlphabeticalByArtist),
            "starred" => Ok(ListType::Starred),
            "byYear" => Ok(ListType::ByYear),
            "byGenre" => Ok(ListType::ByGenre),

            p => Err(SubError::RequiredParameter(format!(
                "{} {} {} is not a known type",
                file!(),
                line!(),
                p
            ))),
        }?;
        let size: u16 = if let Some(size) = req.iter().find(|x| x.0 == "size") {
            size.1
                .parse()
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
        } else {
            10
        };

        let offset: u16 = if let Some(offset) = req.iter().find(|x| x.0 == "offset") {
            offset
                .1
                .parse()
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
        } else {
            0
        };

        let _from_year: Option<u16> =
            if let Some(from_year) = req.iter().find(|x| x.0 == "from_year") {
                Some(from_year.1.parse().map_err(|e| {
                    SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                })?)
            } else {
                None
            };

        let _to_year: Option<u16> =
            if let Some(to_year) = req.iter().find(|x| x.0 == "to_year") {
                Some(to_year.1.parse().map_err(|e| {
                    SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                })?)
            } else {
                None
            };

        let _genre = req
            .iter()
            .find(|x| x.0 == "genre")
            .and_then(|x| Some(x.1.clone()));
        let _music_folder_id = req
            .iter()
            .find(|x| x.0 == "musicFolderId")
            .and_then(|x| Some(x.1.clone()));

        let (songs, album_list) = {
            let conn = SQLITE_CONNECTION.lock().unwrap();
            let songs: Vec<Song> = Song::get_all(&conn)?;
            let album_list = Album::get_all(&conn)?;
            (songs, album_list)
        };

        let mut als: Vec<AlbumList> = album_list
            .into_iter()
            .map(|al| {
                let (earliest_created, song_count) = songs
                    .iter()
                    .filter(|x| x.album.id == al.id)
                    .fold((Utc::now().timestamp(), 0), |(old, y), new| {
                        if old < new.when_added.timestamp() {
                            (old, y + 1)
                        } else {
                            (new.when_added.timestamp(), y + 1)
                        }
                    });
                AlbumList {
                    id: al.id,
                    name: al.title,
                    song_count,
                    created: NaiveDateTime::from_timestamp(earliest_created, 0)
                        .format(DATE_FORMAT)
                        .to_string(),
                    artist: al.artist.name,
                    artist_id: al.artist.id,
                }
            })
            .collect();

        match t {
            ListType::Recent
            | ListType::Starred
            | ListType::ByYear
            | ListType::ByGenre
            | ListType::Frequent
            | ListType::Highest
            | ListType::Newest
            | ListType::Random => als.sort_by(|x, y| x.id.partial_cmp(&y.id).unwrap()),
            ListType::AlphabeticalByName => {
                als.sort_by(|x, y| x.name.partial_cmp(&y.name).unwrap())
            }
            ListType::AlphabeticalByArtist => {
                als.sort_by(|x, y| x.artist.partial_cmp(&y.artist).unwrap())
            }
        };

        Ok(Self {
            albums: als
                .into_iter()
                .skip(offset as usize)
                .take(size as usize)
                .collect(),
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut album_tag = XmlTag::new("albumList");
        for al in self.albums {
            let mut al_tag = XmlTag::new("album");
            al_tag.insert_attrs(map! {
            "id" => al.id,
            "name" => al.name,
            "songCount" => al.song_count,
            "created" => al.created,
            "artistId" => al.artist_id,
            "artist" => al.artist
            });
            album_tag.insert_child(al_tag);
        }
        return_val.insert_child(album_tag);
        return_val.into_string()
    }
}

#[derive(Debug, Copy, Clone)]
enum ListType {
    Random,
    Newest,
    Highest,
    Frequent,
    Recent,
    AlphabeticalByName,
    AlphabeticalByArtist,
    Starred,
    ByYear,
    ByGenre,
}
