use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
pub struct Response {
    songs: Vec<Song>,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        let genre = req
            .iter()
            .find(|x| x.0 == "genre")
            .ok_or_else(|| {
                SubError::RequestedDataNotFound(format!("{} {} genre not found", file!(), line!()))
            })?
            .1
            .clone();
        let count: usize = req
            .iter()
            .find(|x| x.0 == "count")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("10"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let offset: usize = req
            .iter()
            .find(|x| x.0 == "offset")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("0"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(Self {
            songs: {
                let conn = SQLITE_CONNECTION.lock().unwrap();
                Song::get_all(&conn)?
                    .into_iter()
                    .filter(|x| x.genre == genre)
                    .skip(offset)
                    .take(count)
                    .collect()
            },
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut songs_by_genre = XmlTag::new("songsByGenre");
        for s in self.songs {
            let mut song_tag = XmlTag::new("song");
            song_tag.insert_attrs(s.into_xml_map());
            songs_by_genre.insert_child(song_tag);
        }
        return_val.insert_child(songs_by_genre);
        return_val.into_string()
    }
}
