use crate::models::*;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;

use actix_web::HttpResponse;
use std::collections::HashMap;
use std::io::Read;
use std::path::Path;

pub fn download<S: ::std::hash::BuildHasher>(
    req: &HashMap<String, String, S>,
    response_type: ContentType,
) -> HttpResponse {
    match internal_download(&req) {
        Ok(download) => download,
        Err(e) => match response_type {
            ContentType::Xml => {
                let error = XmlTag::error(&e);
                HttpResponse::Ok()
                    .content_type(response_type.to_string())
                    .body(error.into_string())
            }
            ContentType::Json(_func) => unimplemented!(),
        },
    }
}

fn internal_download<S: ::std::hash::BuildHasher>(
    req: &HashMap<String, String, S>,
) -> Result<HttpResponse, SubError> {
    let get_song_id = req
        .get("id")
        .ok_or_else(|| {
            SubError::RequiredParameter(format!("{} {}: id not found", file!(), line!()))
        })?
        .parse::<i64>()
        .map_err(|e| SubError::RequiredParameter(format!("{} {} {}", file!(), line!(), e)))?;
    let song = {
        let conn = SQLITE_CONNECTION.lock().unwrap();
        Song::get_all(&conn)
            .map_err(|e| SubError::GenericError(format!("{} {}: {:?}", file!(), line!(), e)))?
            .into_iter()
            .find(|x| x.id == get_song_id)
            .ok_or_else(|| {
                SubError::RequiredParameter(format!(
                    "{} {}: id {} not found",
                    file!(),
                    line!(),
                    get_song_id
                ))
            })?
    };

    let path = Path::new(song.location.as_str());
    let extension = path
        .extension()
        .ok_or_else(|| {
            SubError::GenericError(format!(
                "{} {}: {} doesn't have an extension",
                file!(),
                line!(),
                song.location
            ))
        })?
        .to_str()
        .ok_or_else(|| {
            SubError::GenericError(format!(
                "{} {}: {} isn't utf-8",
                file!(),
                line!(),
                song.location
            ))
        })?;
    let content_type = extension_to_content_type(extension).ok_or_else(|| {
        SubError::GenericError(format!(
            "{}: {} doesn't have a recognized extension",
            line!(),
            song.location
        ))
    })?;

    let mut f = std::fs::File::open(song.location)
        .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
    let mut data = Vec::new();
    f.read_to_end(&mut data)
        .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

    Ok(HttpResponse::Ok().content_type(content_type).body(data))
}
