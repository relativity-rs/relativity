use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;

pub struct Response {
    _songs: Vec<Song>,
}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        unimplemented!()
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        unimplemented!()
    }
}
