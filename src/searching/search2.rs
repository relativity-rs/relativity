use crate::map;
use crate::models::album::Album;
use crate::models::artists::Artist;
use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;

pub struct Response {
    songs: Vec<Song>,
    albums: Vec<Album>,
    artists: Vec<Artist>,
}

impl New for Response {
    fn new(req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        let (all_albums, all_songs, all_artists) = {
            let conn = &crate::SQLITE_CONNECTION.lock().unwrap();
            let all_albums = Album::get_all(conn)?;
            let all_songs = Song::get_all(conn)?;
            let all_artists = Artist::get_all(conn)?;
            (all_albums, all_songs, all_artists)
        };
        let mut query = req
            .iter()
            .find(|x| x.0 == "query")
            .ok_or_else(|| {
                SubError::RequestedDataNotFound(format!("{} {} genre not found", file!(), line!()))
            })?
            .1
            .to_uppercase();
        if query.ends_with('*') {
            query.pop();
        }
        let artist_count: usize = req
            .iter()
            .find(|x| x.0 == "artistCount")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("10"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let artist_offset: usize = req
            .iter()
            .find(|x| x.0 == "artistOffset")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("0"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        let album_count: usize = req
            .iter()
            .find(|x| x.0 == "albumCount")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("10"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let album_offset: usize = req
            .iter()
            .find(|x| x.0 == "artistOffset")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("0"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        let song_count: usize = req
            .iter()
            .find(|x| x.0 == "songCount")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("10"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let song_offset: usize = req
            .iter()
            .find(|x| x.0 == "artistOffset")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_else(|| String::from("0"))
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        Ok(Self {
            songs: all_songs
                .into_iter()
                .filter(|s| s.title.to_uppercase().contains(&query))
                .skip(song_offset)
                .take(song_count)
                .collect(),
            albums: all_albums
                .into_iter()
                .filter(|s| s.title.to_uppercase().contains(&query))
                .skip(album_offset)
                .take(album_count)
                .collect(),
            artists: all_artists
                .into_iter()
                .filter(|s| s.name.to_uppercase().contains(&query))
                .skip(artist_offset)
                .take(artist_count)
                .collect(),
        })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut search_results_2 = XmlTag::new("searchResult2");
        for ar in self.artists {
            let mut ar_xml = XmlTag::new("artist");
            ar_xml.insert_attrs(map! {
              "id" => ar.id,
              "name" => ar.name
            });
            search_results_2.insert_child(ar_xml);
        }

        for al in self.albums {
            let mut al_xml = XmlTag::new("albums");

            al_xml.insert_attrs(map! {
              "id" => al.id,
              "title" => al.title,
              "parent" =>  "1",
              "artist" => al.artist.name,
              "isDir" => "true"
            });
            search_results_2.insert_child(al_xml);
        }

        for so in self.songs {
            let mut song_xml = XmlTag::new("song");

            song_xml.insert_attrs(so.into_xml_map());
            search_results_2.insert_child(song_xml);
        }

        return_val.insert_child(search_results_2);
        return_val.into_string()
    }
}
