use crate::map;
use crate::models::{Bookmark, User};
use crate::prelude::*;

pub struct Response {
    bookmarks: Vec<Bookmark>,
}

impl New for Response {
    fn new(_req: Vec<(String, String)>, user: User) -> Result<Self, SubError>
    where
        Self: Sized,
    {
        let conn = &crate::SQLITE_CONNECTION.lock().unwrap();
        let bookmarks = Bookmark::get_all(conn)?
            .into_iter()
            .filter(|b| b.user.id == user.id)
            .collect();
        Ok(Self { bookmarks })
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        let mut return_val = XmlTag::ok();
        let mut bookmarks = XmlTag::new("bookmarks");
        for b in self.bookmarks {
            let mut bookmark = XmlTag::new("bookmark");
            bookmark.insert_attrs(map! {"position" => i64::from(b.position),
            "username" => b.user.name,
            "comment" => b.comment});

            let mut entry = XmlTag::new("entry");
            entry.insert_attrs(b.song.into_xml_map());

            bookmark.insert_child(entry);
            bookmarks.insert_child(bookmark);
        }
        return_val.insert_child(bookmarks);
        return_val.into_string()
    }
}
