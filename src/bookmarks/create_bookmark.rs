use crate::diesel::Connection;
use crate::models::bookmark::Bookmark;
use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;

pub struct Response {}

impl New for Response {
    fn new(req: Vec<(String, String)>, user: User) -> Result<Self, SubError> {
        let file_id: i64 = req
            .iter()
            .find(|(key, _)| key == "id")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!(
                    "{} {} id (file_id) not found",
                    file!(),
                    line!()
                ))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let position: i32 = req
            .iter()
            .find(|(key, _)| key == "position")
            .ok_or_else(|| {
                SubError::RequiredParameter(format!("{} {} key not found", file!(), line!()))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let comment = req
            .iter()
            .find(|(key, _)| key == "comment")
            .and_then(|x| Some(x.1.clone()))
            .unwrap_or_default();
        let id = get_hash(&*format!("{}-{}", user.id, file_id), Type::Playlist);

        let conn = &crate::SQLITE_CONNECTION.lock().unwrap();
        conn.transaction::<_, MyError, _>(|| {
            let bookmarks = Bookmark::get_all(conn)?;
            if bookmarks.into_iter().any(|x| x.id == id) {
                Bookmark::delete_by_id(conn, id)?;
            }
            let song = Song::get_all(conn)?
                .into_iter()
                .find(|x| x.id == file_id)
                .ok_or_else(|| {
                    SubError::GenericError(format!(
                        "{} {} song with id {} not found",
                        file!(),
                        line!(),
                        file_id
                    ))
                })?;

            Bookmark {
                id,
                song,
                user,
                position,
                comment,
            }
            .insert(conn)?;
            Ok(())
        })
        .map_err(|e| e.into_sub_error(file!(), line!()))?;
        Ok(Self {})
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
