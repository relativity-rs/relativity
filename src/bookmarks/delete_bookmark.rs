use crate::models::bookmark::Bookmark;
use crate::models::user::User;
use crate::prelude::*;

pub struct Response {}

impl New for Response {
    fn new(req: Vec<(String, String)>, user: User) -> Result<Self, SubError> {
        let file_id: i64 = req
            .iter()
            .find(|(key, _)| key == "id")
            .ok_or_else(|| {
                SubError::GenericError(format!("{} {} id (file_id) not found", file!(), line!()))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let id = get_hash(&*format!("{}-{}", user.id, file_id), Type::Playlist);
        let conn = &crate::SQLITE_CONNECTION.lock().unwrap();
        Bookmark::delete_by_id(conn, id)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(Self {})
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
