use crate::models::{Permission, User};
use crate::prelude::*;

pub struct Response {}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let name = req
            .iter()
            .find(|(id, _)| id == "user")
            .ok_or_else(|| SubError::RequiredParameter(format!("{}: username not found", line!())))?
            .1
            .to_string();
        let password = req
            .iter()
            .find(|(id, _)| id == "password")
            .ok_or_else(|| SubError::GenericError(format!("{}: password not found", line!())))?
            .1
            .to_string();
        let id = get_hash(name.as_ref(), Type::User);
        let u = User {
            name,
            password,
            id,
            permissions: Permission::Regular,
        };
        let conn = &crate::SQLITE_CONNECTION.lock().unwrap();
        u.insert(conn).map_err(|e| {
            SubError::GenericError(format!("{} {} Insert error: {}", file!(), line!(), e))
        })?;
        Ok(Self {})
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
