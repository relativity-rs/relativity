use crate::prelude::*;

use crate::models::User;

pub struct Response {}

impl New for Response {
    fn new(_req: Vec<(String, String)>, _users: User) -> Result<Self, SubError> {
        unimplemented!();
    }
}
impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
