use clap::clap_app;

#[derive(Debug, Default)]
pub struct Configuration {
    pub state_file: String,
    pub music_dir: String,
    pub password_file: String,
    pub mplayer_location: String,
    pub address: String,
}

impl Configuration {
    pub fn new() -> Self {
        let matches = clap_app!(myapp =>
            (version: "0.1.0")
            (author: "Charles Shiller <charles.shiller.1952@gmail.com>")
            (about: "Media library manager")
            (@arg db_file: -b --db_file +takes_value +required "Database file.")
            (@arg music_dir: -m --music_dir +takes_value +required "Music location")
            (@arg password_file: -p --password_file +takes_value + required "Sets the password file")
            (@arg mplayer: -m --mplayer +takes_value + required "Sets mplayer location")
            (@arg address: -a --address +takes_value + required "Set address:port")
        )
        .get_matches();
        let state_file = matches.value_of("db_file").unwrap().to_string();
        let music_dir = matches.value_of("music_dir").unwrap().to_string();
        let password_file = matches.value_of("password_file").unwrap().to_string();
        let mplayer_location = matches.value_of("mplayer").unwrap().to_string();
        let address = matches.value_of("address").unwrap().to_string();
        Self {
            state_file,
            music_dir,
            password_file,
            mplayer_location,
            address,
        }
    }
}
