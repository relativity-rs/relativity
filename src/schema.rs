table! {
    albums (id) {
        id -> BigInt,
        title -> Text,
        artist_id -> BigInt,
    }
}

table! {
    artists (id) {
        id -> BigInt,
        name -> Text,
    }
}

table! {
    playlists (id) {
        id -> BigInt,
        name -> Text,
        songs -> Text,
        date -> Text,
        user_id -> BigInt,
        comment -> Text,
        public -> Integer,
    }
}

table! {
    queue (id) {
        id -> BigInt,
        songs -> Text,
        current -> Text,
        position -> Integer,
    }
}

table! {
    scrobble (id) {
        id -> BigInt,
        last_played -> Integer,
        amount_played -> Integer,
        when_added -> Integer,
        rating -> Integer,
        amount_star -> Integer,
    }
}

table! {
    songs (id) {
        id -> BigInt,
        location -> Text,
        title -> Text,
        album_id -> BigInt,
        year -> Integer,
        track -> Text,
        genre -> Text,
        bitrate -> Integer,
        length -> Integer,
    }
}

table! {
    users (id) {
        id -> BigInt,
        name -> Text,
        password -> Text,
    }
}

table! {
    bookmarks (id) {
        id -> BigInt,
        file_id -> BigInt,
        user -> BigInt,
        position -> Integer,
        comment -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    albums, artists, playlists, queue, scrobble, songs, users, bookmarks,
);
