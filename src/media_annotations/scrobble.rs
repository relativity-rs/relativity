use crate::diesel::Connection;
use crate::models::song::Song;
use crate::models::user::User;
use crate::prelude::*;
use crate::SQLITE_CONNECTION;
use chrono::prelude::*;

pub struct Response {}

impl New for Response {
    fn new(req: Vec<(String, String)>, _user: User) -> Result<Self, SubError> {
        let current_id: i64 = req
            .iter()
            .find(|x| x.0 == "id")
            .ok_or_else(|| {
                SubError::RequestedDataNotFound(format!("{} {} id not found", file!(), line!()))
            })?
            .1
            .parse()
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let current_time = if let Some(current_time) = req.iter().find(|x| x.0 == "time") {
            NaiveDateTime::from_timestamp_opt(
                current_time.1.parse().map_err(|e| {
                    SubError::GenericError(format!(
                        "{} {} {} time {} not string",
                        file!(),
                        line!(),
                        e,
                        current_time.1
                    ))
                })?,
                0,
            )
            .ok_or_else(|| {
                SubError::GenericError(format!(
                    "{} {} can't create time from timestamp {}",
                    file!(),
                    line!(),
                    current_time.1
                ))
            })?
        } else {
            Utc::now().naive_local()
        };

        let conn = SQLITE_CONNECTION.lock().unwrap();
        let mut current_song = Song::get_all(&conn)?
            .into_iter()
            .find(|x| x.id == current_id)
            .ok_or_else(|| {
                SubError::RequestedDataNotFound(format!(
                    "{} {} can't find song with id {}",
                    file!(),
                    line!(),
                    current_id
                ))
            })?;

        (&conn)
            .transaction::<_, MyError, _>(|| {
                current_song.delete(&conn)?;
                current_song.last_played = current_time;
                current_song.amount_played += 1;
                current_song.insert(&conn)?;
                Ok(())
            })
            .map_err(|e| e.into_sub_error(file!(), line!()))?;

        Ok(Self {})
    }
}

impl Printable for Response {
    fn print(self, _response_type: &ContentType) -> String {
        XmlTag::ok().into_string()
    }
}
