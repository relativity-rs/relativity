#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

pub mod album_song_list;
pub mod bookmarks;
pub mod browsing;
pub mod configuration;
pub mod media_annotations;
pub mod media_library_scanning;
pub mod media_retrieval;
pub mod models;
pub mod playlists;
pub mod prelude;
pub mod schema;
pub mod searching;
pub mod system;
pub mod user;

use crate::album_song_list::*;
use crate::bookmarks::*;
use crate::browsing::*;
use crate::configuration::*;
use crate::media_library_scanning::*;
use crate::models::*;
use crate::playlists::*;
use crate::prelude::*;
use crate::searching::*;
use crate::system::*;
use crate::user::*;

use actix_web::{middleware, web, App, HttpRequest, HttpResponse, HttpServer};
use chrono::prelude::*;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::result::Error;
use diesel::sqlite::SqliteConnection;
use rayon::prelude::*;
use std::collections::HashMap;
use std::convert::TryInto;
use std::path::Path;
use std::sync::atomic::{AtomicBool, AtomicI64, Ordering};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::Mutex;
use std::time::SystemTime;
use std::{fs, thread};

#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref CONFIGURATION: Configuration = Configuration::new();
    static ref DATABASE_URL: String = CONFIGURATION.state_file.clone();
    static ref ROOT_DIRECTORY: String = CONFIGURATION.music_dir.clone();
}
const VERSION: &str = "1.16.1";
embed_migrations!("./migrations");

static CUR_COUNT: AtomicI64 = AtomicI64::new(-1);
static IS_SCANNING: AtomicBool = AtomicBool::new(false);

lazy_static! {
    static ref SQLITE_CONNECTION: Mutex<SqliteConnection> = Mutex::new({
        let conn = SqliteConnection::establish(DATABASE_URL.as_ref()).unwrap();
        embedded_migrations::run_with_output(&conn, &mut std::io::stdout()).unwrap();
        conn
    });
}

fn get_nested_files(path: &Path) -> Vec<String> {
    let mut files = vec![];
    let mut dirs = vec![];
    for entry in fs::read_dir(path).unwrap() {
        let entry = entry.unwrap();
        if entry.path().is_dir() {
            dirs.push(entry.path());
        } else {
            let ep = entry.path();
            let location = ep.to_str().unwrap().to_owned();
            if location.to_lowercase().ends_with(".mp3")
                || location.to_lowercase().ends_with(".ogg")
            {
                files.push(location);
            }
        }
    }
    for dir in dirs {
        files.extend_from_slice(&get_nested_files(&dir));
    }
    files
}

fn add_files_in_directory(path: &Path) -> Vec<Song> {
    let files = get_nested_files(path);
    let now = NaiveDateTime::from_timestamp(Utc::now().timestamp(), 0);
    files
        .into_par_iter()
        .filter_map(|x| Some((get_hash_of_song_file(&Path::new(&x)).ok()?, x)))
        .filter_map(|(id, location)| {
            let loc_path = Path::new(&location);
            crate::CUR_COUNT.fetch_add(1, Ordering::Relaxed);
            match taglib::File::new(loc_path) {
                Ok(tl) => match tl.tag() {
                    Ok(tag) => {
                        let suffix = loc_path.extension().unwrap().to_str().unwrap().to_string();
                        let content_type = extension_to_content_type(&suffix)?.to_string();
                        let size = fs::metadata(loc_path).ok()?.len();

                        let title = tag.title().unwrap_or_default();
                        let album_title = tag.album().unwrap_or_default();
                        let artist_name = tag.artist().unwrap_or_default();
                        let album = Album {
                            id: get_hash(
                                format!("album-{}{}", album_title, artist_name).as_str(),
                                Type::Album,
                            ),
                            title: album_title,
                            artist: Artist {
                                id: get_hash(
                                    format!("artist-{}", artist_name).as_str(),
                                    Type::Artist,
                                ),
                                name: artist_name,
                            },
                        };
                        let year = tag
                            .year()
                            .unwrap_or_default()
                            .try_into()
                            .unwrap_or_default();
                        let track = tag.track().unwrap_or_default().to_string();
                        let genre = tag.genre().unwrap_or_default();

                        match tl.audioproperties() {
                            Ok(props) => {
                                let bitrate = props.bitrate().try_into().unwrap_or_default();
                                let length = props.length().try_into().unwrap_or_default();

                                Some(Song {
                                    id,
                                    location: location.to_string(),
                                    title,
                                    album,
                                    year,
                                    track,
                                    genre,
                                    last_played: NaiveDateTime::from_timestamp(0, 0),
                                    when_added: now,
                                    rating: 0,
                                    bitrate,
                                    length,
                                    suffix,
                                    content_type,
                                    size,
                                    amount_played: 0,
                                    amount_star: 0,
                                })
                            }
                            Err(e) => {
                                eprintln!("audio props error: {:?}", e);
                                None
                            }
                        }
                    }
                    Err(e) => {
                        eprintln!("tag error: {:?}", e);
                        None
                    }
                },
                Err(e) => {
                    eprintln!("Not included: {}, {:?}", location, e);
                    None
                }
            }
        })
        .collect()
}

fn main() -> std::io::Result<()> {
    let now = SystemTime::now();
    let (send_update_cache, receive_update_cache): (Sender<bool>, Receiver<bool>) = channel();
    let (send_done_initial_scan, receive_done_initial_scan): (Sender<bool>, Receiver<bool>) =
        channel();

    let songs_exist = {
        let conn = SQLITE_CONNECTION.lock().unwrap();
        !Song::get_all(&conn).unwrap().is_empty()
    };
    if songs_exist {
        send_done_initial_scan.send(true).unwrap();
    }
    thread::spawn(move || {
        let send = send_done_initial_scan.clone();
        let path = Path::new(&*ROOT_DIRECTORY);
        loop {
            println!("Start loop");
            IS_SCANNING.store(true, Ordering::Relaxed);
            let files = add_files_in_directory(path);
            println!("Done building file list");
            println!("Start");
            {
                let conn = SQLITE_CONNECTION.lock().unwrap();
                conn.transaction::<_, Error, _>(|| {
                    Song::remove_all(&conn).unwrap();
                    files.iter().for_each(|x| {
                        let s = x.clone();
                        s.insert(&conn).unwrap();
                    });
                    Bookmark::cleanup(&conn, &files).unwrap();
                    Playlist::cleanup(&conn, &files).unwrap();
                    Song::cleanup(&conn, &files).unwrap();
                    Ok(())
                })
                .unwrap();
            }
            println!("Done");
            IS_SCANNING.store(false, Ordering::Relaxed);
            CUR_COUNT.store(-1, Ordering::Relaxed);
            if !songs_exist {
                send.send(true).unwrap();
            }
            receive_update_cache.recv().unwrap();
        }
    });
    if !songs_exist {
        receive_done_initial_scan.recv().unwrap();
    }
    send_update_cache.send(true).unwrap();
    println!("{}", now.elapsed().unwrap().as_secs());
    std::env::set_var("RUST_LOG", "actix_server=debug,actix_web=debug");
    env_logger::init();
    println!("Started!");

    let auth_middleware_internal =
        { move |cmd, form, query, chan_send| auth_middleware(cmd, query, form, chan_send) };
    let auth_middleware_post = {
        let chan_send = send_update_cache.clone();
        move |cmd| {
            let chan_send = chan_send.clone();
            move |query, form| auth_middleware_internal(cmd, form, query, chan_send.clone())
        }
    };
    let auth_middleware_get = {
        let chan_send = send_update_cache.clone();
        move |cmd| {
            let chan_send = chan_send.clone();
            move |query| auth_middleware_internal(cmd, None, query, chan_send.clone())
        }
    };
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .service(
                web::resource("/rest/ping.view")
                    .route(web::post().to(auth_middleware_post(Command::Ping)))
                    .route(web::get().to(auth_middleware_get(Command::Ping))),
            )
            .service(
                web::resource("/rest/ping")
                    .route(web::post().to(auth_middleware_post(Command::Ping)))
                    .route(web::get().to(auth_middleware_get(Command::Ping))),
            )
 
            .service(
                web::resource("/rest/getLicense.view")
                    .route(web::post().to(auth_middleware_post(Command::GetLicense)))
                    .route(web::get().to(auth_middleware_get(Command::GetLicense))),
            )
            .service(
                web::resource("/rest/getLicense")
                    .route(web::post().to(auth_middleware_post(Command::GetLicense)))
                    .route(web::get().to(auth_middleware_get(Command::GetLicense))),
            )

            .service(
                web::resource("/rest/getIndexes.view")
                    .route(web::post().to(auth_middleware_post(Command::GetIndexes)))
                    .route(web::get().to(auth_middleware_get(Command::GetIndexes))),
            )
            .service(
                web::resource("/rest/getIndexes")
                    .route(web::post().to(auth_middleware_post(Command::GetIndexes)))
                    .route(web::get().to(auth_middleware_get(Command::GetIndexes))),
            )
          .service(
                web::resource("/rest/getMusicFolders.view")
                    .route(web::post().to(auth_middleware_post(Command::GetMusicFolders)))
                    .route(web::get().to(auth_middleware_get(Command::GetMusicFolders))),
            )
           .service(
                web::resource("/rest/getMusicFolders.view")
                    .route(web::post().to(auth_middleware_post(Command::GetMusicFolders)))
                    .route(web::get().to(auth_middleware_get(Command::GetMusicFolders))),
            )
           .service(
                web::resource("/rest/getMusicDirectory")
                    .route(web::post().to(auth_middleware_post(Command::GetMusicDirectory)))
                    .route(web::get().to(auth_middleware_get(Command::GetMusicDirectory))),
            )
            .service(
                web::resource("/rest/getGenres.view")
                    .route(web::post().to(auth_middleware_post(Command::GetGenres)))
                    .route(web::get().to(auth_middleware_get(Command::GetGenres))),
            )
             .service(
                web::resource("/rest/getGenres")
                    .route(web::post().to(auth_middleware_post(Command::GetGenres)))
                    .route(web::get().to(auth_middleware_get(Command::GetGenres))),
            )
           .service(
                web::resource("/rest/getArtists.view")
                    .route(web::post().to(auth_middleware_post(Command::GetArtists)))
                    .route(web::get().to(auth_middleware_get(Command::GetArtists))),
            )
            .service(
                web::resource("/rest/getArtists")
                    .route(web::post().to(auth_middleware_post(Command::GetArtists)))
                    .route(web::get().to(auth_middleware_get(Command::GetArtists))),
            )
           .service(
                web::resource("/rest/getArtist.view")
                    .route(web::post().to(auth_middleware_post(Command::GetArtist)))
                    .route(web::get().to(auth_middleware_get(Command::GetArtist))),
            )
            .service(
                web::resource("/rest/getArtist")
                    .route(web::post().to(auth_middleware_post(Command::GetArtist)))
                    .route(web::get().to(auth_middleware_get(Command::GetArtist))),
            )
            .service(
                web::resource("/rest/stream.view")
                    .route(web::post().to(auth_middleware_post(Command::Stream)))
                    .route(web::get().to(auth_middleware_get(Command::Stream))),
            )
             .service(
                web::resource("/rest/stream")
                    .route(web::post().to(auth_middleware_post(Command::Stream)))
                    .route(web::get().to(auth_middleware_get(Command::Stream))),
            )
           .service(
                web::resource("/rest/search.view")
                    .route(web::post().to(auth_middleware_post(Command::Search)))
                    .route(web::get().to(auth_middleware_get(Command::Search))),
            )
            .service(
                web::resource("/rest/search")
                    .route(web::post().to(auth_middleware_post(Command::Search)))
                    .route(web::get().to(auth_middleware_get(Command::Search))),
            )
            .service(
                web::resource("/rest/search2.view")
                    .route(web::post().to(auth_middleware_post(Command::Search2)))
                    .route(web::get().to(auth_middleware_get(Command::Search2))),
            )
             .service(
                web::resource("/rest/search2")
                    .route(web::post().to(auth_middleware_post(Command::Search2)))
                    .route(web::get().to(auth_middleware_get(Command::Search2))),
            )
           .service(
                web::resource("/rest/search3.view")
                    .route(web::post().to(auth_middleware_post(Command::Search3)))
                    .route(web::get().to(auth_middleware_get(Command::Search3))),
            )
            .service(
                web::resource("/rest/search3")
                    .route(web::post().to(auth_middleware_post(Command::Search3)))
                    .route(web::get().to(auth_middleware_get(Command::Search3))),
            )
           .service(
                web::resource("/rest/download.view")
                    .route(web::post().to(auth_middleware_post(Command::Download)))
                    .route(web::get().to(auth_middleware_get(Command::Download))),
            )
            .service(
                web::resource("/rest/download")
                    .route(web::post().to(auth_middleware_post(Command::Download)))
                    .route(web::get().to(auth_middleware_get(Command::Download))),
            )
            .service(
                web::resource("/rest/getAlbum.view")
                    .route(web::post().to(auth_middleware_post(Command::GetAlbum)))
                    .route(web::get().to(auth_middleware_get(Command::GetAlbum))),
            )
             .service(
                web::resource("/rest/getAlbum")
                    .route(web::post().to(auth_middleware_post(Command::GetAlbum)))
                    .route(web::get().to(auth_middleware_get(Command::GetAlbum))),
            )
            .service(
                web::resource("/rest/getSong.view")
                    .route(web::post().to(auth_middleware_post(Command::GetSong)))
                    .route(web::get().to(auth_middleware_get(Command::GetSong))),
            )
             .service(
                web::resource("/rest/getSong")
                    .route(web::post().to(auth_middleware_post(Command::GetSong)))
                    .route(web::get().to(auth_middleware_get(Command::GetSong))),
            )
           .service(
                web::resource("/rest/createPlaylist.view")
                    .route(web::post().to(auth_middleware_post(Command::CreatePlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::CreatePlaylist))),
            )
            .service(
                web::resource("/rest/createPlaylist")
                    .route(web::post().to(auth_middleware_post(Command::CreatePlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::CreatePlaylist))),
            )
            .service(
                web::resource("/rest/deletePlaylist.view")
                    .route(web::post().to(auth_middleware_post(Command::DeletePlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::DeletePlaylist))),
            )
             .service(
                web::resource("/rest/deletePlaylist")
                    .route(web::post().to(auth_middleware_post(Command::DeletePlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::DeletePlaylist))),
            )
            .service(
                web::resource("/rest/getPlaylists.view")
                    .route(web::post().to(auth_middleware_post(Command::GetPlaylists)))
                    .route(web::get().to(auth_middleware_get(Command::GetPlaylists))),
            )
             .service(
                web::resource("/rest/getPlaylists")
                    .route(web::post().to(auth_middleware_post(Command::GetPlaylists)))
                    .route(web::get().to(auth_middleware_get(Command::GetPlaylists))),
            )
            .service(
                web::resource("/rest/getPlaylist.view")
                    .route(web::post().to(auth_middleware_post(Command::GetPlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::GetPlaylist))),
            )
             .service(
                web::resource("/rest/getPlaylist")
                    .route(web::post().to(auth_middleware_post(Command::GetPlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::GetPlaylist))),
            )
            .service(
                web::resource("/rest/updatePlaylist.view")
                    .route(web::post().to(auth_middleware_post(Command::UpdatePlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::UpdatePlaylist))),
            )
             .service(
                web::resource("/rest/updatePlaylist")
                    .route(web::post().to(auth_middleware_post(Command::UpdatePlaylist)))
                    .route(web::get().to(auth_middleware_get(Command::UpdatePlaylist))),
            )
            .service(
                web::resource("/rest/getAlbumList.view")
                    .route(web::post().to(auth_middleware_post(Command::GetAlbumList)))
                    .route(web::get().to(auth_middleware_get(Command::GetAlbumList))),
            )
             .service(
                web::resource("/rest/getAlbumList")
                    .route(web::post().to(auth_middleware_post(Command::GetAlbumList)))
                    .route(web::get().to(auth_middleware_get(Command::GetAlbumList))),
            )
            .service(
                web::resource("/rest/getAlbumList2.view")
                    .route(web::post().to(auth_middleware_post(Command::GetAlbumList2)))
                    .route(web::get().to(auth_middleware_get(Command::GetAlbumList2))),
            )
             .service(
                web::resource("/rest/getAlbumList2")
                    .route(web::post().to(auth_middleware_post(Command::GetAlbumList2)))
                    .route(web::get().to(auth_middleware_get(Command::GetAlbumList2))),
            )
            .service(
                web::resource("/rest/getRandomSongs.view")
                    .route(web::post().to(auth_middleware_post(Command::GetRandomSongs)))
                    .route(web::get().to(auth_middleware_get(Command::GetRandomSongs))),
            )
             .service(
                web::resource("/rest/getRandomSongs")
                    .route(web::post().to(auth_middleware_post(Command::GetRandomSongs)))
                    .route(web::get().to(auth_middleware_get(Command::GetRandomSongs))),
            )
            .service(
                web::resource("/rest/getSongsByGenre.view")
                    .route(web::post().to(auth_middleware_post(Command::GetSongsByGenre)))
                    .route(web::get().to(auth_middleware_get(Command::GetSongsByGenre))),
            )
             .service(
                web::resource("/rest/getSongsByGenre")
                    .route(web::post().to(auth_middleware_post(Command::GetSongsByGenre)))
                    .route(web::get().to(auth_middleware_get(Command::GetSongsByGenre))),
            )
            .service(
                web::resource("/rest/scrobble.view")
                    .route(web::post().to(auth_middleware_post(Command::Scrobble)))
                    .route(web::get().to(auth_middleware_post(Command::Scrobble))),
            )
             .service(
                web::resource("/rest/scrobble")
                    .route(web::post().to(auth_middleware_post(Command::Scrobble)))
                    .route(web::get().to(auth_middleware_post(Command::Scrobble))),
            )
            .service(
                web::resource("/rest/getScanStatus.view")
                    .route(web::post().to(auth_middleware_post(Command::GetScanStatus)))
                    .route(web::get().to(auth_middleware_get(Command::GetScanStatus))),
            )
             .service(
                web::resource("/rest/getScanStatus")
                    .route(web::post().to(auth_middleware_post(Command::GetScanStatus)))
                    .route(web::get().to(auth_middleware_get(Command::GetScanStatus))),
            )
            .service(
                web::resource("/rest/getBookmarks.view")
                    .route(web::post().to(auth_middleware_post(Command::GetBookmarks)))
                    .route(web::get().to(auth_middleware_get(Command::GetBookmarks))),
            )
             .service(
                web::resource("/rest/getBookmarks")
                    .route(web::post().to(auth_middleware_post(Command::GetBookmarks)))
                    .route(web::get().to(auth_middleware_get(Command::GetBookmarks))),
            )
            .service(
                web::resource("/rest/createBookmarks.view")
                    .route(web::post().to(auth_middleware_post(Command::CreateBookmark)))
                    .route(web::get().to(auth_middleware_get(Command::CreateBookmark))),
            )
             .service(
                web::resource("/rest/createBookmarks")
                    .route(web::post().to(auth_middleware_post(Command::CreateBookmark)))
                    .route(web::get().to(auth_middleware_get(Command::CreateBookmark))),
            )
            .service(
                web::resource("/rest/deleteBookmark.view")
                    .route(web::post().to(auth_middleware_post(Command::DeleteBookmark)))
                    .route(web::get().to(auth_middleware_get(Command::DeleteBookmark))),
            )
             .service(
                web::resource("/rest/deleteBookmark")
                    .route(web::post().to(auth_middleware_post(Command::DeleteBookmark)))
                    .route(web::get().to(auth_middleware_get(Command::DeleteBookmark))),
            )
            .service(
                web::resource("/rest/startScan.view")
                    .route(web::post().to(auth_middleware_post(Command::StartScan)))
                    .route(web::get().to(auth_middleware_get(Command::StartScan))),
            )
             .service(
                web::resource("/rest/startScan")
                    .route(web::post().to(auth_middleware_post(Command::StartScan)))
                    .route(web::get().to(auth_middleware_get(Command::StartScan))),
            )
            //This is a fill-in to add manipulate users
            .service(
                web::resource("/rest/addUser.view")
                    .route(web::post().to(auth_middleware_post(Command::AddUser)))
                    .route(web::get().to(auth_middleware_get(Command::AddUser))),
            )
             .service(
                web::resource("/rest/addUser")
                    .route(web::post().to(auth_middleware_post(Command::AddUser)))
                    .route(web::get().to(auth_middleware_get(Command::AddUser))),
            )
            .service(
                web::resource("/rest/deleteUser.view")
                    .route(web::post().to(auth_middleware_post(Command::DeleteUser)))
                    .route(web::get().to(auth_middleware_get(Command::DeleteUser))))
             .service(
                web::resource("/rest/deleteUser")
                    .route(web::post().to(auth_middleware_post(Command::DeleteUser)))
                    .route(web::get().to(auth_middleware_get(Command::DeleteUser))
            )
           )
    })
    .bind(&CONFIGURATION.address)?
    .run()

}

fn auth_middleware(
    command: Command,
    request: HttpRequest,
    form: Option<web::Form<Vec<(String, String)>>>,
    send_update_cache: Sender<bool>,
) -> HttpResponse {
    let query: web::Query<Vec<(String, String)>> =
        web::Query::from_query(request.query_string()).unwrap();
    let mut req: Vec<(String, String)> =
        query.iter().map(|(a, b)| (a.clone(), b.clone())).collect();
    if let Some(form) = form {
        req.extend(form.iter().map(|(a, b)| (a.clone(), b.clone())));
    };
    println!("{:?}", req);

    let req_hash: HashMap<String, String> =
        req.iter().map(|(x, y)| (x.clone(), y.clone())).collect();
    let mut response_type = (|| match req_hash.iter().find(|x| *x.0 == "f") {
        Some((_key, val)) => match val.as_ref() {
            "xml" => Some(ContentType::Xml),
            "json" => Some(ContentType::Json(String::from(""))),
            "jsonp" => Some(ContentType::Json(req_hash.get("callback")?.clone())),
            p => panic!(format!("error: {}", p)),
        },
        None => Some(ContentType::Xml),
    })();

    if response_type == None {
        eprintln!("{} {} callback name not found", file!(), line!());
        response_type = Some(ContentType::Xml)
    };
    let response_type = response_type.unwrap();

    if command == Command::Stream || command == Command::Download {
        media_retrieval::download::download(&req_hash, response_type)
    } else {
        let output = match mch(command, &response_type, &req_hash, req, &send_update_cache) {
            Ok(output) => (output),
            Err(e) => {
                eprintln!("{} {} {:?}", file!(), line!(), e);
                match response_type {
                    ContentType::Xml => {
                        let return_val = XmlTag::error(&e);
                        return_val.into_string().into_bytes()
                    }
                    ContentType::Json(ref func) => {
                        let return_val = JsonTag::error(&e);

                        return_val.into_string(func).into_bytes()
                    }
                }
            }
        };
        if response_type == ContentType::Xml {
            println!("{}", String::from_utf8_lossy(&output));
        }
        println!("{}", response_type.to_string());
        HttpResponse::Ok()
            .content_type(response_type.to_string())
            .body(output)
    }
}

fn build_and_print<A>(
    response_type: &ContentType,
    req: Vec<(String, String)>,
    user: User,
) -> Result<String, SubError>
where
    A: New,
    A: Printable,
{
    Ok(A::new(req, user)?.print(response_type))
}

fn mch(
    command: Command,
    response_type: &ContentType,
    req: &HashMap<String, String>,
    req_vec: Vec<(String, String)>,
    send_update_cache: &Sender<bool>,
) -> Result<Vec<u8>, SubError> {
    let all_users = {
        let conn = SQLITE_CONNECTION.lock().unwrap();
        User::get_all(&conn)?
    };
    let cur_user = req
        .get("u")
        .ok_or_else(|| {
            SubError::RequiredParameter(format!("{} {} user not supplied", file!(), line!()))
        })?
        .to_string();
    let user = all_users
        .into_iter()
        .find(|x| x.name == cur_user)
        .ok_or(SubError::WrongUsernameOrPassword)?;
    match command {
        command => {
            let text = match command {
                Command::Ping => Ok(ping::Response::new().print(&response_type)),
                Command::GetLicense => Ok(get_license::Response::new().print(&response_type)),
                Command::GetIndexes => {
                    build_and_print::<get_indexes::Response>(response_type, req_vec, user)
                }
                Command::GetMusicDirectory => {
                    build_and_print::<get_music_directory::Response>(response_type, req_vec, user)
                }
                Command::GetMusicFolders => {
                    build_and_print::<get_music_folders::Response>(response_type, req_vec, user)
                }
                Command::GetGenres => {
                    build_and_print::<get_genres::Response>(response_type, req_vec, user)
                }
                Command::GetArtists => {
                    build_and_print::<get_artists::Response>(response_type, req_vec, user)
                }
                Command::GetArtist => {
                    build_and_print::<get_artist::Response>(response_type, req_vec, user)
                }
                Command::GetAlbum => {
                    build_and_print::<get_album::Response>(response_type, req_vec, user)
                }
                Command::GetSong => {
                    build_and_print::<get_song::Response>(response_type, req_vec, user)
                }
                Command::CreatePlaylist => {
                    build_and_print::<create_playlist::Response>(response_type, req_vec, user)
                }
                Command::GetPlaylists => {
                    build_and_print::<get_playlists::Response>(response_type, req_vec, user)
                }
                Command::UpdatePlaylist => {
                    build_and_print::<update_playlist::Response>(response_type, req_vec, user)
                }
                Command::GetPlaylist => {
                    build_and_print::<get_playlist::Response>(response_type, req_vec, user)
                }
                Command::DeletePlaylist => {
                    build_and_print::<delete_playlist::Response>(response_type, req_vec, user)
                }
                Command::Scrobble => {
                    build_and_print::<crate::media_annotations::scrobble::Response>(
                        response_type,
                        req_vec,
                        user,
                    )
                }
                Command::GetAlbumList => {
                    build_and_print::<get_album_list::Response>(response_type, req_vec, user)
                }
                Command::GetRandomSongs => {
                    build_and_print::<get_random_songs::Response>(response_type, req_vec, user)
                }
                Command::GetAlbumList2 => {
                    build_and_print::<get_album_list2::Response>(response_type, req_vec, user)
                }
                Command::GetSongsByGenre => {
                    build_and_print::<get_songs_by_genre::Response>(response_type, req_vec, user)
                }
                Command::GetScanStatus => {
                    build_and_print::<get_scan_status::Response>(response_type, req_vec, user)
                }
                Command::GetBookmarks => {
                    build_and_print::<get_bookmarks::Response>(response_type, req_vec, user)
                }
                Command::CreateBookmark => {
                    build_and_print::<create_bookmark::Response>(response_type, req_vec, user)
                }
                Command::DeleteBookmark => {
                    build_and_print::<delete_bookmark::Response>(response_type, req_vec, user)
                }
                Command::StartScan => {
                    Ok(
                        start_scan::Response::new(&req_vec, &user, &send_update_cache)?
                            .print(response_type),
                    )
                }

                Command::AddUser => {
                    build_and_print::<add_user::Response>(response_type, req_vec, user)
                }
                Command::DeleteUser => {
                    build_and_print::<delete_user::Response>(response_type, req_vec, user)
                }

                Command::Search => {
                    build_and_print::<search::Response>(response_type, req_vec, user)
                }
                Command::Search2 => {
                    build_and_print::<search2::Response>(response_type, req_vec, user)
                }
                Command::Search3 => {
                    build_and_print::<search3::Response>(response_type, req_vec, user)
                }
                Command::Stream | Command::Download => unreachable!(),
            }?;
            Ok(text.into_bytes())
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq)]
enum Command {
    //System
    Ping,
    GetLicense,

    //Browsing
    GetIndexes,
    GetMusicFolders,
    GetMusicDirectory,
    GetGenres,
    GetArtists,
    GetArtist,
    GetAlbum,
    GetSong,

    //Album/song list
    GetAlbumList,
    GetAlbumList2,
    GetRandomSongs,
    GetSongsByGenre,

    //Searching
    Search,
    Search2,
    Search3,

    //Playlists
    GetPlaylists,
    GetPlaylist,
    CreatePlaylist,
    DeletePlaylist,
    UpdatePlaylist,

    //Media retrieval
    Stream,
    Download,

    //Media annotations
    Scrobble,

    //Bookmarks
    GetBookmarks,
    CreateBookmark,
    DeleteBookmark,

    //Media Library Scanning
    GetScanStatus,
    StartScan,

    // Fill-in
    AddUser,
    DeleteUser,
}
