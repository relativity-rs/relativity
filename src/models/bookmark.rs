use crate::models::Song;
use crate::models::User;

use crate::diesel::ExpressionMethods;
use crate::prelude::*;
use crate::schema::bookmarks;
use diesel::query_dsl::{QueryDsl, RunQueryDsl};
use diesel::SqliteConnection;
use std::sync::MutexGuard;
#[derive(Queryable, Insertable, Debug)]
#[table_name = "bookmarks"]
struct BookmarkDb {
    id: i64,
    file_id: i64,
    user: i64,
    position: i32,
    comment: String,
}

#[derive(Debug, Clone)]
pub struct Bookmark {
    pub id: i64,
    pub song: Song,
    pub user: User,
    pub position: i32,
    pub comment: String,
}

impl Bookmark {
    pub fn get_all(conn: &MutexGuard<SqliteConnection>) -> Result<Vec<Self>, SubError> {
        use crate::schema::bookmarks::dsl::*;

        let users = User::get_all(conn)?;
        let songs = Song::get_all(conn)?;
        bookmarks
            .load::<BookmarkDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
            .into_iter()
            .map(|x| {
                Ok(Self {
                    song: songs
                        .iter()
                        .find(|s| s.id == x.file_id)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} song {} not found",
                                file!(),
                                line!(),
                                x.file_id
                            ))
                        })?
                        .clone(),
                    id: x.id,
                    user: users
                        .iter()
                        .find(|u| u.id == x.id)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} user with id {} not found",
                                file!(),
                                line!(),
                                x.id
                            ))
                        })?
                        .clone(),
                    position: x.position,
                    comment: x.comment,
                })
            })
            .collect::<Result<Vec<_>, SubError>>()
    }

    pub fn insert(&self, conn: &MutexGuard<SqliteConnection>) -> Result<(), diesel::result::Error> {
        let bookmarks_db = BookmarkDb {
            file_id: self.song.id,
            id: self.id,
            user: self.user.id,
            position: self.position,
            comment: self.comment.clone(),
        };

        diesel::insert_or_ignore_into(bookmarks::table)
            .values(bookmarks_db)
            .execute(&**conn)?;
        Ok(())
    }
    pub fn delete_by_id(
        conn: &MutexGuard<SqliteConnection>,
        id: i64,
    ) -> Result<(), diesel::result::Error> {
        diesel::delete(bookmarks::table.filter(bookmarks::columns::id.eq(id))).execute(&**conn)?;
        Ok(())
    }

    pub fn cleanup(
        conn: &MutexGuard<SqliteConnection>,
        songs_which_exists: &[Song],
    ) -> Result<(), String> {
        for s in songs_which_exists {
            diesel::delete(
                bookmarks::table.filter(crate::schema::bookmarks::columns::file_id.ne(s.id)),
            )
            .execute(&**conn)
            .map_err(|e| format!("{} {} {}", file!(), line!(), e))?;
        }
        Ok(())
    }
}
