use crate::prelude::*;
use diesel::SqliteConnection;
use std::collections::HashMap;
use std::sync::MutexGuard;

use crate::schema::artists;
use diesel::query_dsl::RunQueryDsl;

#[derive(Queryable, Insertable, Debug)]
#[table_name = "artists"]
struct ArtistDb {
    id: i64,
    name: String,
}
#[derive(Debug, Clone, Default)]
pub struct Artist {
    pub id: i64,
    pub name: String,
}

impl Artist {
    pub fn get_all(conn: &MutexGuard<SqliteConnection>) -> Result<Vec<Self>, SubError> {
        use crate::schema::artists::dsl::*;

        let mut artist_db = HashMap::new();
        for i in artists
            .load::<ArtistDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
        {
            artist_db.insert(i.id, i);
        }

        Ok(artist_db
            .into_iter()
            .map(|x| {
                let i = x.1;
                Self {
                    id: i.id,
                    name: i.name.clone(),
                }
            })
            .collect())
    }
    pub fn insert(&self, conn: &MutexGuard<SqliteConnection>) -> Result<(), diesel::result::Error> {
        let artist_db = ArtistDb {
            id: self.id,
            name: self.name.to_string(),
        };

        diesel::insert_or_ignore_into(artists::table)
            .values(artist_db)
            .execute(&**conn)?;
        Ok(())
    }
}
