use diesel::query_dsl::RunQueryDsl;
use diesel::SqliteConnection;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::sync::MutexGuard;

use crate::prelude::*;
use crate::schema::users;

#[derive(Queryable, Insertable, Debug)]
#[table_name = "users"]
struct UserDb {
    id: i64,
    name: String,
    password: String,
}
#[derive(Debug, Clone)]
pub struct User {
    pub id: i64,
    pub name: String,
    pub password: String,
    pub permissions: Permission,
}

impl User {
    pub fn get_all(conn: &MutexGuard<SqliteConnection>) -> Result<Vec<Self>, SubError> {
        use crate::schema::users::dsl::*;
        let file = BufReader::new(
            File::open(&crate::CONFIGURATION.password_file)
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?,
        );
        let it = file.lines().map(|x| {
            let x =
                x.map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
            let mut split = x.split('|');
            let user_name = split.next().ok_or_else(|| {
                SubError::GenericError(format!("{} {} username not found", file!(), line!()))
            })?;
            let loc_id = get_hash(user_name, Type::User);
            let loc_password = split.next().ok_or_else(|| {
                SubError::GenericError(format!("{} {} password not found", file!(), line!()))
            })?;
            Ok(Self {
                id: loc_id,
                password: loc_password.to_string(),
                name: user_name.to_string(),
                permissions: Permission::Admin,
            })
        });
        users
            .load::<UserDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
            .into_iter()
            .map(|x| {
                Ok(Self {
                    id: x.id,
                    name: x.name,
                    password: x.password,
                    permissions: Permission::Regular,
                })
            })
            .chain(it)
            .collect()
    }

    pub fn insert(&self, conn: &MutexGuard<SqliteConnection>) -> Result<(), diesel::result::Error> {
        let user_db = UserDb {
            id: self.id,
            name: self.name.clone(),
            password: self.password.clone(),
        };

        diesel::insert_into(users::table)
            .values(user_db)
            .execute(&**conn)?;
        Ok(())
    }
}
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Permission {
    Admin,
    Regular,
}
