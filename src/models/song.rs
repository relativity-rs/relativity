use crate::prelude::*;
use chrono::prelude::*;
use diesel::query_dsl::{QueryDsl, RunQueryDsl};
use diesel::SqliteConnection;
use std::collections::HashMap;
use std::convert::TryInto;
use std::fs;
use std::path::Path;
use std::sync::MutexGuard;

use crate::map;

use crate::diesel::ExpressionMethods;
use crate::models::album::Album;
use crate::prelude::extension_to_content_type;

use crate::schema::{scrobble, songs};
use chrono::NaiveDateTime;

#[derive(Queryable, Insertable, Debug)]
#[table_name = "songs"]
struct SongDb {
    id: i64,
    location: String,
    title: String,
    album_id: i64,
    year: i32,
    track: String,
    genre: String,
    bitrate: i32,
    length: i32,
}

#[derive(Queryable, Insertable, Debug)]
#[table_name = "scrobble"]
struct ScrobbleDb {
    id: i64,
    last_played: i32,
    amount_played: i32,
    when_added: i32,
    rating: i32,
    amount_star: i32,
}

#[derive(Debug, Clone)]
pub struct Song {
    pub id: i64,
    pub location: String,
    pub title: String,
    pub album: Album,
    pub year: i32,
    pub track: String,
    pub genre: String,
    pub last_played: NaiveDateTime,
    pub when_added: NaiveDateTime,
    pub rating: i32,
    pub bitrate: i32,
    pub length: i32,
    pub suffix: String,
    pub content_type: String,
    pub size: u64,
    pub amount_played: i32,
    pub amount_star: i32,
}

impl Default for Song {
    fn default() -> Self {
        Self {
            id: 0,
            location: "".to_string(),
            title: "".to_string(),
            album: crate::models::album::Album::default(),
            year: 0,
            track: "".to_string(),
            genre: "".to_string(),
            last_played: NaiveDateTime::from_timestamp(Utc::now().timestamp(), 0),
            when_added: NaiveDateTime::from_timestamp(Utc::now().timestamp(), 0),
            rating: 0,
            bitrate: 0,
            length: 0,
            suffix: "".to_string(),
            content_type: "".to_string(),
            size: 0,
            amount_played: 0,
            amount_star: 0,
        }
    }
}

impl Song {
    pub fn remove_all(conn: &MutexGuard<SqliteConnection>) -> Result<(), SubError> {
        diesel::delete(songs::table)
            .execute(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        Ok(())
    }
    pub fn insert(self, conn: &MutexGuard<SqliteConnection>) -> Result<(), diesel::result::Error> {
        let song_db = SongDb {
            id: self.id,
            location: self.location,
            title: self.title,
            album_id: self.album.id,
            year: self.year,
            track: self.track,
            genre: self.genre,
            length: self.length,
            bitrate: self.bitrate,
        };

        let scrobble_db = ScrobbleDb {
            id: self.id,
            last_played: self.last_played.timestamp().try_into().unwrap_or_default(),
            when_added: self.when_added.timestamp().try_into().unwrap_or_default(),
            amount_played: self.amount_played.try_into().unwrap_or_default(),
            rating: self.rating,
            amount_star: self.amount_star.try_into().unwrap_or_default(),
        };
        diesel::insert_or_ignore_into(scrobble::table)
            .values(scrobble_db)
            .execute(&**conn)?;
        self.album.insert(conn)?;
        diesel::insert_or_ignore_into(songs::table)
            .values(song_db)
            .execute(&**conn)?;
        Ok(())
    }

    pub fn get_all(conn: &MutexGuard<SqliteConnection>) -> Result<Vec<Self>, SubError> {
        Ok(Self::get_iter(conn)?.collect::<Result<Vec<Self>, SubError>>()?)
    }

    pub fn get_iter(
        conn: &MutexGuard<SqliteConnection>,
    ) -> Result<impl Iterator<Item = Result<Self, SubError>>, SubError> {
        use crate::schema::scrobble::dsl::*;
        use crate::schema::songs::dsl::*;

        let scrobble_db = scrobble
            .load::<ScrobbleDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let all_albums = Album::get_all(conn)?;

        Ok(songs
            .load::<SongDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
            .into_iter()
            .map(move |song_db| {
                let cur_album = all_albums
                    .iter()
                    .find(|a| a.id == song_db.album_id)
                    .ok_or_else(|| {
                        SubError::GenericError(format!(
                            "{} {} No album with id of {}",
                            file!(),
                            line!(),
                            song_db.album_id
                        ))
                    })?
                    .clone();
                let current_scrobble =
                    scrobble_db
                        .iter()
                        .find(|s| s.id == song_db.id)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} cannot find last_played for id {}",
                                file!(),
                                line!(),
                                song_db.id
                            ))
                        })?;
                let last_played_ndt =
                    NaiveDateTime::from_timestamp_opt(current_scrobble.last_played.into(), 0)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} timestamp {} cannot be converted ",
                                file!(),
                                line!(),
                                current_scrobble.last_played
                            ))
                        })?;
                let when_added_ndt =
                    NaiveDateTime::from_timestamp_opt(current_scrobble.when_added.into(), 0)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} timestamp {} cannot be converted ",
                                file!(),
                                line!(),
                                current_scrobble.when_added
                            ))
                        })?;

                let loc_path = Path::new(&song_db.location);
                let suffix = loc_path.extension().unwrap().to_str().unwrap().to_string();
                let content_type = extension_to_content_type(&suffix)
                    .ok_or_else(|| {
                        SubError::GenericError(format!("{} {} bad content type", file!(), line!()))
                    })?
                    .to_string();
                let size = fs::metadata(loc_path)
                    .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
                    .len();
                Ok(Self {
                    id: song_db.id,
                    location: song_db.location,
                    title: song_db.title,
                    album: cur_album,
                    year: song_db.year,
                    track: song_db.track.clone(),
                    genre: song_db.genre.clone(),
                    last_played: last_played_ndt,
                    when_added: when_added_ndt,

                    bitrate: song_db.bitrate,
                    length: song_db.length,
                    suffix,
                    content_type,
                    size,
                    amount_played: current_scrobble.amount_played,
                    amount_star: current_scrobble.amount_star,
                    rating: current_scrobble.rating,
                })
            }))
    }

    pub fn update_last_played(
        self,
        conn: &MutexGuard<SqliteConnection>,
        last_played_ndt: NaiveDateTime,
    ) -> Result<(), SubError> {
        use crate::schema::scrobble::dsl::*;
        let timestamp: i32 = last_played_ndt.timestamp().try_into().unwrap_or_default();

        diesel::update(scrobble.filter(id.eq(self.id)))
            .set(last_played.eq(timestamp))
            .execute(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(())
    }

    pub fn rating(self, conn: &MutexGuard<SqliteConnection>, rate: i32) -> Result<(), SubError> {
        use crate::schema::scrobble::dsl::*;

        diesel::update(scrobble.filter(id.eq(self.id)))
            .set(rating.eq(rate))
            .execute(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(())
    }

    pub fn get_suffix(&self) -> String {
        Path::new(&self.location)
            .extension()
            .unwrap_or_default()
            .to_str()
            .unwrap_or_default()
            .to_string()
    }
    pub fn get_content_type(&self) -> Result<&'static str, SubError> {
        let suffix = Path::new(&self.location)
            .extension()
            .unwrap_or_default()
            .to_str()
            .unwrap_or_default();
        Ok(extension_to_content_type(suffix).ok_or_else(|| {
            SubError::GenericError(format!("{} {}: {}", file!(), line!(), self.location))
        })?)
    }

    pub fn get_file_size(&self) -> Result<u64, SubError> {
        let loc = Path::new(&self.location);
        let md = fs::metadata(loc).map_err(|e| {
            SubError::GenericError(format!(
                "{} {}: {} : {}",
                file!(),
                line!(),
                e,
                loc.display()
            ))
        })?;
        Ok(md.len())
    }

    pub fn into_xml_map(self) -> HashMap<&'static str, SafeString> {
        map! {
                    "id" => self.id,
                    "title" => self.title,
                    "album" => self.album.title,
                    "artist"=> self.album.artist.name,
                    "track" => if self.track != "" { self.track} else {String::from("0")},
                    "year" => self.year.to_string(),
                    "genre" => self.genre,
                    "path" => self.location,
                    "contentType" => self.content_type,
                    "suffix" => self.suffix,
                    "size" => self.size.to_string()
        }
    }

    pub fn delete(&self, conn: &MutexGuard<SqliteConnection>) -> Result<(), SubError> {
        diesel::delete(
            crate::schema::scrobble::table.filter(crate::schema::scrobble::id.eq(self.id)),
        )
        .execute(&**conn)
        .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;

        diesel::delete(songs::table.filter(songs::id.eq(self.id)))
            .execute(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(())
    }

    pub fn cleanup(
        conn: &MutexGuard<SqliteConnection>,
        songs_which_exists: &[Self],
    ) -> Result<(), SubError> {
        use crate::schema::scrobble::dsl::*;
        let mut scrobbles = scrobble
            .load::<ScrobbleDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        let ids: Vec<i64> = songs_which_exists.iter().map(|x| x.id).collect();

        for x in &mut scrobbles {
            if !ids.contains(&x.id) {
                diesel::delete(
                    crate::schema::scrobble::table.filter(crate::schema::scrobble::id.eq(x.id)),
                )
                .execute(&**conn)
                .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
            }
        }
        Ok(())
    }
}
