use crate::models::artists::Artist;
use crate::prelude::*;
use crate::schema::albums;
use diesel::query_dsl::RunQueryDsl;
use diesel::SqliteConnection;
use std::sync::MutexGuard;

#[derive(Queryable, Insertable, Debug)]
#[table_name = "albums"]
struct AlbumDb {
    id: i64,
    title: String,
    artist_id: i64,
}

#[derive(Debug, Clone, Default)]
pub struct Album {
    pub id: i64,
    pub title: String,
    pub artist: Artist,
}

impl Album {
    pub fn get_all(conn: &MutexGuard<SqliteConnection>) -> Result<Vec<Self>, SubError> {
        Ok(Self::get_iter(conn)?.collect::<Result<Vec<Self>, SubError>>()?)
    }
    pub fn get_iter(
        conn: &MutexGuard<SqliteConnection>,
    ) -> Result<impl Iterator<Item = Result<Self, SubError>>, SubError> {
        use crate::schema::albums::dsl::*;

        let artist_db = Artist::get_all(conn)?;

        Ok(albums
            .load::<AlbumDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?
            .into_iter()
            .map(move |x| {
                let cur_album_db = x;
                let cur_artist_db = artist_db
                    .iter()
                    .find(|x_a| x_a.id == cur_album_db.artist_id)
                    .ok_or_else(|| SubError::GenericError(format!("{} {}", file!(), line!())))?
                    .clone();
                Ok(Self {
                    id: cur_album_db.id,
                    title: cur_album_db.title,
                    artist: Artist {
                        id: cur_artist_db.id,
                        name: cur_artist_db.name.clone(),
                    },
                })
            }))
    }
    pub fn insert(&self, conn: &MutexGuard<SqliteConnection>) -> Result<(), diesel::result::Error> {
        let album_db = AlbumDb {
            id: self.id,
            title: self.title.to_string(),
            artist_id: self.artist.id,
        };

        self.artist.insert(conn)?;
        diesel::insert_or_ignore_into(albums::table)
            .values(album_db)
            .execute(&**conn)?;
        Ok(())
    }
}
