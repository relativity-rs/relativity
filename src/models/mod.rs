pub mod album;
pub mod artists;
pub mod bookmark;
pub mod playlist;
pub mod song;
pub mod user;

pub use crate::models::album::*;
pub use crate::models::artists::*;
pub use crate::models::bookmark::*;
pub use crate::models::playlist::*;
pub use crate::models::song::*;
pub use crate::models::user::*;
