use crate::diesel::ExpressionMethods;
use crate::models::song::Song;
use crate::models::user::Permission;
use crate::models::user::User;
use crate::prelude::*;
use crate::schema::playlists;
use chrono::NaiveDateTime;
use diesel::query_dsl::{QueryDsl, RunQueryDsl};
use diesel::SqliteConnection;
use std::sync::MutexGuard;

#[derive(Queryable, Insertable, Debug)]
#[table_name = "playlists"]
struct PlaylistDb {
    id: i64,
    name: String,
    songs: String,
    date: String,
    user_id: i64,
    comment: String,
    public: i32,
}

#[derive(Debug, Clone)]
pub struct Playlist {
    pub id: i64,
    pub name: String,
    pub songs: Vec<Song>,
    pub date: chrono::NaiveDateTime,
    pub user: User,
    pub comment: String,
    pub public: bool,
}

impl Playlist {
    pub fn cleanup(
        conn: &MutexGuard<SqliteConnection>,
        songs_which_exists: &[Song],
    ) -> Result<(), SubError> {
        let mut pls = Self::get_all(conn)?;
        let ids: Vec<i64> = songs_which_exists.iter().map(|x| x.id).collect();

        for x in &mut pls {
            x.songs.retain(|s| ids.contains(&s.id));
            Self::delete_by_playlist_id(conn, x.id)?;
            x.insert(conn)?;
        }
        Ok(())
    }
    pub fn get_all(conn: &MutexGuard<SqliteConnection>) -> Result<Vec<Self>, SubError> {
        use crate::schema::playlists::dsl::*;

        let users_list = User::get_all(conn)?;
        let songs_list = Song::get_all(conn)?;
        playlists
            .load::<PlaylistDb>(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {}", line!(), e)))?
            .into_iter()
            .map(|x| {
                Ok(Self {
                    user: users_list
                        .iter()
                        .find(|user| x.user_id == user.id)
                        .ok_or_else(|| {
                            SubError::GenericError(format!(
                                "{} {} user id not found",
                                file!(),
                                line!()
                            ))
                        })?
                        .clone(),
                    songs: x
                        .songs
                        .split(',')
                        .collect::<Vec<_>>()
                        .iter()
                        .map(|s| {
                            songs_list
                                .iter()
                                .find(|x| x.id == s.parse::<i64>().unwrap())
                        })
                        .filter_map(|s| {
                            if s.is_some() {
                                Some(s.unwrap().clone())
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<_>>(),
                    id: x.id,
                    name: x.name,
                    public: x.public == 1,
                    comment: x.comment,

                    date: NaiveDateTime::parse_from_str(&x.date, DATE_FORMAT).map_err(|e| {
                        SubError::GenericError(format!("{} {} {}", file!(), line!(), e))
                    })?,
                })
            })
            .collect::<Result<Vec<Self>, SubError>>()
    }

    pub fn has_permission_by_name(&self, user: &str) -> bool {
        self.user.name == user || self.user.permissions == Permission::Admin
    }

    pub fn insert(&self, conn: &MutexGuard<SqliteConnection>) -> Result<(), SubError> {
        let playlist_db = PlaylistDb {
            id: self.id,
            name: self.name.clone(),
            songs: self
                .songs
                .iter()
                .map(|x| x.id.to_string())
                .collect::<Vec<_>>()
                .join(","),
            date: self.date.format(DATE_FORMAT).to_string(),
            user_id: self.user.id,
            comment: self.comment.clone(),
            public: if self.public { 1 } else { 0 },
        };

        diesel::insert_into(playlists::table)
            .values(playlist_db)
            .execute(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(())
    }

    pub fn delete_by_playlist_id(
        conn: &MutexGuard<SqliteConnection>,
        playlist_id: i64,
    ) -> Result<(), SubError> {
        diesel::delete(playlists::table.filter(playlists::id.eq(playlist_id)))
            .execute(&**conn)
            .map_err(|e| SubError::GenericError(format!("{} {} {}", file!(), line!(), e)))?;
        Ok(())
    }
}
