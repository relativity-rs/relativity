pub mod ping {
    use crate::prelude::*;

    #[derive(Debug, Default)]
    pub struct Response {}

    impl Response {
        pub fn new() -> Self {
            Self::default()
        }
    }

    impl Printable for Response {
        fn print(self, response_type: &ContentType) -> String {
            match response_type {
                ContentType::Xml => XmlTag::ok().into_string(),
                ContentType::Json(f) => JsonTag::ok(vec![]).into_string(f),
            }
        }
    }
}

pub mod get_license {
    use crate::map;
    use crate::prelude::*;
    use serde_json::json;
    #[derive(Debug, Default)]
    pub struct Response {}

    impl Response {
        pub fn new() -> Self {
            Self::default()
        }
    }

    impl Printable for Response {
        fn print(self, response_type: &ContentType) -> String {
            match response_type {
                ContentType::Xml => {let mut top_level = XmlTag::ok();

                    let mut license = XmlTag::new("license");
                    license.insert_attrs(map!{"valid" => "true", "email" =>"foo@bar.com", "licenseExpires"=>"9999-12-29T23:23:59"});
                    top_level.insert_child(license);
                    top_level.into_string()}
                ContentType::Json(func) => {
                    JsonTag::ok(vec![("license".into(), json!({"valid": true, "email": "foo@bar.com", "trialExpires": "9999-12-29T23:23:59"}))]).into_string(func)
                }
            }
        }
    }
}
