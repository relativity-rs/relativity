use crate::models::User;
use byteorder::{LittleEndian, ReadBytesExt};
use chrono::NaiveDateTime;
use crypto::digest::Digest;
use crypto::sha2::Sha512;

use serde_json::json;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::{Cursor, Read};

use std::path;
#[macro_export]
macro_rules! map (
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, crate::prelude::SafeString::from($value));
            )+
            m
        }
     };
);

const MASK: i64 = (i64::max_value() << 8) >> 2;
const INVERT_MASK: i64 = !MASK;

#[derive(Debug, Eq, PartialEq)]
pub enum Type {
    TopLevel,
    Song,
    Album,
    Artist,
    Playlist,
    Queue,
    User,
    Bookmark,
}

impl Into<i64> for Type {
    fn into(self) -> i64 {
        match self {
            Type::TopLevel => 1,
            Type::Song => 2,
            Type::Album => 3,
            Type::Artist => 4,
            Type::Playlist => 5,
            Type::Queue => 6,
            Type::User => 7,
            Type::Bookmark => 8,
        }
    }
}

impl TryFrom<i64> for Type {
    type Error = SubError;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value & INVERT_MASK {
            1 => Ok(Type::TopLevel),
            2 => Ok(Type::Song),
            3 => Ok(Type::Album),
            4 => Ok(Type::Artist),
            5 => Ok(Type::Playlist),
            6 => Ok(Type::Queue),
            7 => Ok(Type::User),
            8 => Ok(Type::Bookmark),
            s => Err(SubError::GenericError(format!(
                "{} {} {} doesn't have a known type",
                file!(),
                line!(),
                s
            ))),
        }
    }
}

pub fn get_hash_of_song_file(location: &path::Path) -> Result<i64, String> {
    let mut sha512 = Sha512::new();
    let mut f = File::open(location).map_err(|e| format!("{} {} {}", file!(), line!(), e))?;
    sha512.input(location.to_string_lossy().into_owned().as_bytes());
    let mut buf: [u8; 1024 * 1024] = [0 as u8; 1024 * 1024];
    loop {
        let s = f
            .read(&mut buf)
            .map_err(|e| format!("{} {} {}", file!(), line!(), e))?;
        if s == 0 {
            break;
        }
        sha512.input(&buf);
    }
    let res = sha512.result_str();

    let num = Cursor::new(res.as_bytes())
        .read_i64::<LittleEndian>()
        .unwrap()
        >> 2;

    let num = num & MASK;
    assert!(num > 0);
    let t: i64 = Type::Song.into();
    Ok(num + t)
}

pub fn get_hash<'a, T: Into<&'a str>>(orig: T, id_type: Type) -> i64 {
    let mut sha512 = Sha512::new();
    sha512.input_str(orig.into());
    let res = sha512.result_str();

    let num = Cursor::new(res.as_bytes())
        .read_i64::<LittleEndian>()
        .unwrap()
        >> 2;

    let num: i64 = num & MASK;
    assert!(num > 0);
    let t: i64 = id_type.into();
    num + t
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum ContentType {
    Xml,
    Json(String),
}

impl ContentType {
    pub fn to_string(&self) -> &'static str {
        match self {
            Self::Xml => "text/xml; charset=utf-8",
            Self::Json(val) => match val.as_ref() {
                "" => "application/json; charset=utf-8",
                _ => "application/javascript; charset=utf-8",
            },
        }
    }
}

pub trait Printable {
    fn print(self, response_type: &ContentType) -> String;
}

pub trait New {
    fn new(req: Vec<(String, String)>, user: User) -> Result<Self, SubError>
    where
        Self: Sized;
}

pub fn extension_to_content_type(string: &str) -> Option<&'static str> {
    match string.to_lowercase().as_ref() {
        "mp3" => Some("audio/mp3"),
        "ogg" => Some("audio/ogg"),
        &_ => None,
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SafeString {
    inner: String,
}

impl SafeString {
    pub fn to_str(&self) -> &str {
        &self.inner
    }
    pub fn into_string(self) -> String {
        self.inner
    }
}

impl From<&str> for SafeString {
    fn from(orig: &str) -> Self {
        Self {
            inner: "\"".to_owned()
                + &orig
                    .replace("\"", "&quot;")
                    .replace("&", "&amp;")
                    .replace("'", "&apos;")
                    .replace("<", "&lt;")
                    .replace(">", "&gt;")
                + "\"",
        }
    }
}

pub const DATE_FORMAT: &str = "%Y-%m-%dT%H-%M-%S";

impl From<NaiveDateTime> for SafeString {
    fn from(orig: NaiveDateTime) -> Self {
        Self {
            inner: "\"".to_owned() + &orig.format(DATE_FORMAT).to_string() + "\"",
        }
    }
}

impl From<String> for SafeString {
    fn from(orig: String) -> Self {
        Self::from(orig.as_ref())
    }
}

impl From<i64> for SafeString {
    fn from(orig: i64) -> Self {
        Self {
            inner: "\"".to_owned() + &orig.to_string() + "\"",
        }
    }
}

impl From<u64> for SafeString {
    fn from(orig: u64) -> Self {
        Self {
            inner: "\"".to_owned() + &orig.to_string() + "\"",
        }
    }
}

impl From<u32> for SafeString {
    fn from(orig: u32) -> Self {
        Self {
            inner: "\"".to_owned() + &orig.to_string() + "\"",
        }
    }
}

impl From<u8> for SafeString {
    fn from(orig: u8) -> Self {
        Self {
            inner: "\"".to_owned() + &orig.to_string() + "\"",
        }
    }
}

impl From<bool> for SafeString {
    fn from(orig: bool) -> Self {
        Self {
            inner: "\"".to_owned() + &orig.to_string() + "\"",
        }
    }
}

#[derive(Debug, Clone)]
pub struct XmlTag {
    tag: &'static str,
    attrs: HashMap<&'static str, SafeString>,
    children: Vec<XmlTag>,
    val: Option<SafeString>,
}

impl XmlTag {
    pub fn new(tag: &'static str) -> Self {
        Self {
            tag,
            attrs: HashMap::new(),
            children: Vec::new(),
            val: None,
        }
    }
    pub fn ok() -> Self {
        let mut return_val = Self::new("subsonic-response");
        return_val.insert_attr("status", "ok");
        return_val.insert_attr("version", crate::VERSION);
        return_val
    }

    pub fn error(e: &SubError) -> Self {
        let mut return_val = Self::new("subsonic-response");
        return_val.insert_attr("xmlns", "http://subsonic.org/restapi");
        return_val.insert_attr("status", "failed");
        return_val.insert_attr("version", crate::VERSION);

        let mut child = Self::new("error");
        let s: String = e.clone().into();
        let n: i64 = e.clone().into();
        child.insert_attrs(map! {"code" => n, "message" => s});
        return_val.insert_child(child);
        return_val
    }

    pub fn insert_attr<T: Into<SafeString>>(&mut self, key: &'static str, val: T) {
        self.attrs.insert(key, val.into());
    }

    pub fn insert_attrs(&mut self, map: HashMap<&'static str, SafeString>) {
        self.attrs.extend(map);
    }

    pub fn insert_val<T: Into<SafeString>>(&mut self, val: T) {
        self.val = Some(val.into());
    }

    pub fn insert_child(&mut self, child: Self) {
        self.children.push(child);
    }

    pub fn into_string(self) -> String {
        let mut return_val = String::new();
        return_val += r#"<?xml version="1.0" encoding="UTF-8"?>"#;

        return_val += &("<".to_owned() + self.tag);
        for (key, val) in self.attrs {
            return_val += &(" ".to_owned() + key + "=" + val.to_str());
        }
        if self.children.is_empty() && self.val.is_none() {
            return_val += "/>";
        } else {
            return_val += ">";
            if !self.children.is_empty() {
                return_val += &self
                    .children
                    .into_iter()
                    .fold(String::new(), |old, new| old + &new.into_string_lower());
            }

            return_val += &self.val.map(|v| v.into_string()).unwrap_or_default();
            return_val += &("</".to_owned() + self.tag + ">");
        }
        return_val
    }
    fn into_string_lower(self) -> String {
        let mut return_val = String::new();

        return_val += &("<".to_owned() + self.tag);
        for (key, val) in self.attrs {
            return_val += &(" ".to_owned() + key + "=" + val.to_str());
        }
        if self.children.is_empty() && self.val.is_none() {
            return_val += "/>";
        } else {
            return_val += ">";
            if !self.children.is_empty() {
                return_val += &self
                    .children
                    .into_iter()
                    .fold(String::new(), |old, new| old + &new.into_string_lower());
            }

            return_val += &self.val.map(|v| v.into_string()).unwrap_or_default();
            return_val += &("</".to_owned() + self.tag + ">");
        }
        return_val
    }
}

#[derive(Debug)]
pub enum MyError {
    SubError(SubError),
    Diesel(diesel::result::Error),
}

impl From<SubError> for MyError {
    fn from(val: SubError) -> Self {
        Self::SubError(val)
    }
}

impl From<diesel::result::Error> for MyError {
    fn from(val: diesel::result::Error) -> Self {
        Self::Diesel(val)
    }
}

impl MyError {
    pub fn into_sub_error(self, file: &'static str, line: u32) -> SubError {
        match self {
            Self::SubError(a) => a,
            Self::Diesel(d) => SubError::GenericError(format!("{} {} {}", file, line, d)),
        }
    }
}

#[derive(Debug, Clone)]
pub enum SubError {
    GenericError(String),
    RequiredParameter(String),
    ClientMustUpgrade,
    ServerMustUpgrade,
    WrongUsernameOrPassword,
    TokenAuthenticationError,
    UserNotAuthorized,
    TrialOver,
    RequestedDataNotFound(String),
}

impl Into<XmlTag> for SubError {
    fn into(self) -> XmlTag {
        XmlTag::error(&self)
    }
}

impl Into<i64> for SubError {
    fn into(self) -> i64 {
        match self {
            SubError::GenericError(_) => 0,
            SubError::RequiredParameter(_) => 10,
            SubError::ClientMustUpgrade => 20,
            SubError::ServerMustUpgrade => 30,
            SubError::WrongUsernameOrPassword => 40,
            SubError::TokenAuthenticationError => 41,
            SubError::UserNotAuthorized => 50,
            SubError::TrialOver => 60,
            SubError::RequestedDataNotFound(_) => 70,
        }
    }
}

impl Into<String> for SubError {
    fn into(self) -> String {
        match self {
            SubError::GenericError(s)
            | SubError::RequiredParameter(s)
            | SubError::RequestedDataNotFound(s) => s,
            SubError::ClientMustUpgrade
            | SubError::WrongUsernameOrPassword
            | SubError::TokenAuthenticationError
            | SubError::ServerMustUpgrade
            | SubError::UserNotAuthorized
            | SubError::TrialOver => "".into(),
        }
    }
}

#[derive(Debug)]
pub struct JsonTag {
    status: String,
    child: Vec<(String, serde_json::Value)>,
}

impl JsonTag {
    pub fn error(e: &SubError) -> JsonTag {
        let code: i64 = e.clone().into();
        let message: String = e.clone().into();
        JsonTag {
            status: "failed".into(),
            child: vec![("error".into(), json!({"code": code, "message": message}))],
        }
    }

    pub fn ok(child: Vec<(String, serde_json::Value)>) -> JsonTag {
        JsonTag {
            status: "ok".into(),
            child,
        }
    }

    pub fn into_string(self, func: &str) -> String {
        let mut subsonic_response = serde_json::Map::new();
        subsonic_response.insert("status".into(), serde_json::Value::String(self.status));
        subsonic_response.insert(
            "version".into(),
            serde_json::Value::String(crate::VERSION.into()),
        );
        for (key, value) in self.child {
            subsonic_response.insert(key, value);
        }

        let mut return_map = serde_json::Map::new();

        return_map.insert(
            "subsonic-response".into(),
            serde_json::Value::Object(subsonic_response.into()),
        );
        let return_val = serde_json::Value::Object(return_map.into());
        if func == "" {
            return_val.to_string()
        } else {
            format!("{}({});", func, return_val.to_string())
        }
    }
}
